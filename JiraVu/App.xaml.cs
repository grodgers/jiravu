﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using JiraVu.Loggers;

namespace JiraVu
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            string filePath = @"C:\jiravu.txt";

            if (System.IO.File.Exists(filePath))
            {
                string[] lines = System.IO.File.ReadAllLines(filePath);
                JiraVu.Properties.Settings.Default.RestBaseUrl = lines[0];
                JiraVu.Properties.Settings.Default.DefaultUsername = lines[1];
            }
        }
    }
}
