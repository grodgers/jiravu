﻿using System;
using System.Collections;
using System.Collections.Generic;
using JiraRestClient;

namespace JiraVu.Converters
{
    public static class FieldValueConverter
    {
        public static object Convert(JiraIssueTypeFieldMetaSchema schema, object toConvert)
        {
            return Convert(schema, toConvert, null, null);
        }

        public static object Convert(JiraIssueTypeFieldMetaSchema schema, object toConvert, string xParam)
        {
            return Convert(schema, toConvert, xParam, null);
        }

        public static object Convert(JiraIssueTypeFieldMetaSchema schema, object toConvert, string xParam, string yParam)
        {
            if (toConvert == null) return "";

            object objToConvert = toConvert;

            switch (schema.type)
            {
                case "string":
                    objToConvert = ReplaceParams(toConvert.ToString(), xParam, yParam);
                    break;
                case "array":
                    if (schema.system != null)
                    {
                        switch (schema.items)
                        {
                            case "version":
                                if (toConvert.GetType().Name == "ArrayList")
                                {
                                    // Just copy the list over
                                }
                                else
                                {   // Convert any user-supplied information into JIRA Version objects
                                    objToConvert = new List<JiraVersion>();
                                    foreach (string ver in toConvert.ToString().Split(','))
                                        ((List<JiraVersion>)objToConvert).Add(new JiraVersion(ver.Trim()));
                                }
                                break;
                            default:
                                objToConvert = ReplaceParamsInArray((ArrayList)toConvert, xParam, yParam);
                                break;
                        }
                    }
                    else if (schema.custom != null)
                    {
                        objToConvert = ReplaceParamsInArray((ArrayList)toConvert, xParam, yParam);
                    }
                    break;
            }

            return objToConvert;
        }

        public static string ReplaceParams(string origValue, string xParam, string yParam)
        {
            if (!String.IsNullOrEmpty(xParam))
            {
                origValue = origValue.Replace("{X}", xParam);
                origValue = origValue.Replace("{x}", xParam);
            }
            if (!String.IsNullOrEmpty(yParam))
            {
                origValue = origValue.Replace("{Y}", yParam);
                origValue = origValue.Replace("{y}", yParam);
            }
            return origValue;
        }

        private static ArrayList ReplaceParamsInArray(ArrayList objToConvert, string xParam, string yParam)
        {
            for (int i = 0; i < objToConvert.Count; i++)
            {
                objToConvert[i] = ReplaceParams(objToConvert[i].ToString(), xParam, yParam);
            }
            return objToConvert;
        }
    }
}
