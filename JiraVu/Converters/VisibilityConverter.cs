﻿using System;
using JiraRestClient;

namespace JiraVu.Converters
{
    public class VisibilityConverter : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            // Return the opposite of the current value
            if (System.Convert.ToBoolean(value))
                return System.Windows.Visibility.Visible;
            else 
                return System.Windows.Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
