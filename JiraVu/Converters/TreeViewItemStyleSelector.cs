﻿using System;
using System.Windows;
using System.Windows.Controls;
using JiraVu.Core;

namespace JiraVu.Converters
{
    public class TreeViewItemStyleSelector : StyleSelector
    {
        public override Style SelectStyle(object item, DependencyObject container)
        {
            var element = container as FrameworkElement;

            if (element == null)
            {
                return null;
            }

            if (item is FilterFolder)
            {
                return element.FindResource("FilterFolderStyle") as Style;
            }
            else if (item is JiraVuIssue)
            {
                return element.FindResource("JiraVuIssueStyle") as Style;
            }
            else
            {
                return null;
            }
        }
    }
}
