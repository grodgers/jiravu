﻿using System;
using JiraRestClient;

namespace JiraVu.Converters
{
    public class BooleanConverter : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            // Return the opposite of the current value
            return !System.Convert.ToBoolean(value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            // Return the opposite of the current value
            return !System.Convert.ToBoolean(value);
        }
    }
}