﻿using System;
using System.Windows;
using System.Windows.Controls;
using JiraVu.Core;

namespace JiraVu.Converters
{
    public class TreeViewTemplateSelector : DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            var element = container as FrameworkElement;

            if (element == null)
            {
                return null;
            }

            if (item is FilterFolder)
            {
                return element.FindResource("FilterFolderTemplate") as DataTemplate;
            }
            else if (item is JiraVuIssue)
            {
                return element.FindResource("JiraVuIssueTemplate") as DataTemplate;
            }
            else
            {
                return null;
            }
        }
    }
}
