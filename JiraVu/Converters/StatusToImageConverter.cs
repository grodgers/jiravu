﻿using System;

namespace JiraVu.Converters
{
    public class StatusToImageConverter : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Enums.OperationStates status = (Enums.OperationStates)value;

            switch (status)
            {
                case Enums.OperationStates.InProcess:
                    return "../Images/blue-arrow-right-16x16.png";
                case Enums.OperationStates.Success:
                    return "../Images/green-checkmark-16x16.png";
                case Enums.OperationStates.Failure:
                    return "../Images/red-x-16x16.png";
                default:
                    return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}
