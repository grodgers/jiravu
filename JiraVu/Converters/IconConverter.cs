﻿using System;
using System.IO;
using System.Reflection;

namespace JiraVu.Converters
{
    public class IconConverter : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string iconUrl = value.ToString();
            string iconFileName = iconUrl.Substring(iconUrl.LastIndexOf('/') + 1);
            string currDir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            if (File.Exists(Path.Combine(currDir, "icons", iconFileName)))
            {
                iconUrl = Path.Combine(currDir, "icons", iconFileName);
            }

            return iconUrl;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}
