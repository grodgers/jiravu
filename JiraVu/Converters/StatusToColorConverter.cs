﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;
using JiraRestClient;
using JiraVu.Core;

namespace JiraVu.Converters
{
    class StatusToColorConverter : IValueConverter
    {
        public static readonly IValueConverter Instance = new StatusToColorConverter();
        
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Dictionary<string, JiraVuStatusColor> statusColors = (Dictionary<string, JiraVuStatusColor>)parameter;
            BrushConverter conv = new BrushConverter();
            string statusName = value.ToString();

            return conv.ConvertFrom(statusColors[statusName].Color.ToString());
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}
