﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using JiraRestClient;
using JiraVu.Views;
using JiraVu.Core;
using JiraVu.Loggers;
using Xceed.Wpf.Toolkit;

namespace JiraVu
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private JiraRestClient.JiraRestClient _client = null;
        private List<string> operationsList = new List<string>();
        private string _windowTitle;

        #region Public Properties

        public JiraRestClient.JiraRestClient RestClient
        {
            get
            {
                if (_client == null) throw new Exception("JiraRestClient is not available.");
                return _client;
            }
            set
            {
                _client = value;
            }
        }

        public Button BackButton { get { return this.btnBack; } }
        public Button NextButton { get { return this.btnNext; } }
        public Dictionary<string, JiraVuStatusColor> StatusColors { get; set; }
        public JiraIssueCreateMetaResponse IssueCreationMetadata { get; set; }
        public JiraVuIssueCreationMeta SelectedIssueCreationMeta { get; set; }
        public JiraVuIssueType SelectedIssueType { get; set; }
        public List<JiraVuIssue> SelectedIssues { get; set; }
        public List<JiraVuIssueType> SelectedIssueTypes { get; set; }
        public List<string> SelectedIssueTypeNames { get; set; }
        public List<string> SelectedProjectKeys { get; set; }
        public string CurrentJqlSearch { get; set; }
        public string UserNotification
        {
            get
            {
                return this.tbUserNotification.Text;
            }
            set
            {
                this.tbUserNotification.Text = value;
            }
        }
        public string WindowTitle
        {
            get
            {
                return "JiraVu";
            }
            set
            {
                _windowTitle = value; RaisePropertyChanged("WindowTitle");
            }
        }

        #endregion

        #region Constructors

        public MainWindow()
        {
            InitializeComponent();
        }

        #endregion

        #region Public Methods

        public List<JiraVuIssueType> GetDistinctIssueTypeMeta()
        {
            if (this.IssueCreationMetadata == null) return null;

            List<string> namesAdded = new List<string>();
            List<JiraVuIssueType> listToReturn = new List<JiraVuIssueType>();
            foreach (JiraProject project in this.IssueCreationMetadata.projects)
            {
                foreach (JiraIssueType issueType in project.issuetypes)
                {
                    JiraVuIssueType jvIssueType = new JiraVuIssueType(issueType);
                    if (!namesAdded.Contains(jvIssueType.JiraIssueType.name))
                    {
                        namesAdded.Add(jvIssueType.JiraIssueType.name);
                        listToReturn.Add(jvIssueType);
                    }
                }
            }

            return listToReturn;
        }

        public void HideUserNotification()
        {
            this.tbUserNotification.Visibility = Visibility.Collapsed;
        }

        public void MoveBack()
        {
            INavigationUserControl currentChild = (INavigationUserControl)grdUserControls.Children[0];
            if (currentChild.PrepMoveBack())
            {
                grdUserControls.Children.Clear();
                grdUserControls.Children.Add(currentChild.BackUserControl);
            }
        }

        public void MoveNext()
        {
            INavigationUserControl currentChild = (INavigationUserControl)grdUserControls.Children[0];

            if (currentChild.DoWorkAsync())
            {
                if (currentChild.PrepMoveNext())
                {
                    grdUserControls.Children.Clear();
                    grdUserControls.Children.Add(currentChild.NextUserControl);
                }
            }
        }

        public void ShowUserNotification(string userNotificationText)
        {
            UserNotification = userNotificationText;
            this.tbUserNotification.Visibility = Visibility.Visible;
        }

        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion INotifyPropertyChanged

        #region Event Handlers

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            MoveBack();
        }

        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
            MoveNext();
        }

        private void mainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                grdUserControls.Children.Clear();
                grdUserControls.Children.Add(new JiraRestAuthUserControl(this));
                RaisePropertyChanged("WindowTitle");
            }
            catch (Exception ex) { LogHelper.LogException(ex); }
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            if (grdUserControls.Children.Count < 1) return;
            if (grdUserControls.Children[0] is IssueTreeViewUserControl)
            {
                IssueTreeViewUserControl childUC = grdUserControls.Children[0] as IssueTreeViewUserControl;
                if (childUC.PromptForSave() == MessageBoxResult.Cancel)
                {
                    e.Cancel = true;
                }
            }
        }

        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.X && Keyboard.Modifiers.HasFlag(ModifierKeys.Control | ModifierKeys.Shift))
            {
                if (btnNext.IsEnabled) MoveNext();
            }
            else if (e.Key == Key.Z && Keyboard.Modifiers.HasFlag(ModifierKeys.Control | ModifierKeys.Shift))
            {
                if (btnBack.IsEnabled) MoveNext();
            }
        }

        #endregion

    }
}
