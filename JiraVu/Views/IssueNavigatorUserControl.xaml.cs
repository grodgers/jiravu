﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using JiraRestClient;
using JiraVu.Core;
using Xceed.Wpf.Toolkit;

namespace JiraVu.Views
{
    /// <summary>
    /// Interaction logic for IssueNavigatorUserControl.xaml
    /// </summary>
    public partial class IssueNavigatorUserControl : UserControl, INavigationUserControl
    {
        private List<JiraVuIssue> jiraVuIssues = new List<JiraVuIssue>();
        private UserControl _ucBackControl;
        private UserControl _ucNextControl;

        #region Public Properties

        public MainWindow ParentWindow { get; set; }

        public UserControl BackUserControl
        {
            get
            {
                if (_ucBackControl == null)
                    _ucBackControl = new MainMenuUserControl(ParentWindow);
                return _ucBackControl;
            }
            set
            {
                _ucBackControl = value;
            }
        }

        public UserControl NextUserControl
        {
            get
            {   // If not overridden, the next screen in the flow is to display issue operations
                if (_ucNextControl == null) _ucNextControl = new IssueOperationsUserControl(this.ParentWindow);
                return _ucNextControl;
            }
            set
            {
                _ucNextControl = value;
            }
        }

        #endregion

        #region Constructors

        public IssueNavigatorUserControl(MainWindow parentWindow)
        {
            InitializeComponent();
            this.ParentWindow = parentWindow;
        }
        
        #endregion

        #region Public Methods

        public bool DoWorkAsync()
        {
            // Collect all metadata related to creating JIRA issues for each project
            ParentWindow.IssueCreationMetadata = ParentWindow.RestClient.GetCreateIssueMeta();
            // Collect all of the issues that were selected for an operation
            List<JiraVuIssue> selectedIssues = new List<JiraVuIssue>();
            ParentWindow.SelectedIssueCreationMeta = new JiraVuIssueCreationMeta();
            foreach (JiraVuIssue issue in jiraVuIssues)
            {
                if (issue.IsSelected)
                {
                    string projectKey = issue.JiraIssue.fields.project.key;
                    string issueTypeName = issue.JiraIssue.fields.issuetype.name;
                    // Add the issue itself to the selected list
                    selectedIssues.Add(issue);
                    // Collect metadata specifically for selected issues
                    if (ParentWindow.SelectedIssueCreationMeta.GetProject(projectKey) == null)
                    {   // Add the project and issue type metadata
                        JiraVuProject newProject = new JiraVuProject(ParentWindow.IssueCreationMetadata.GetProject(projectKey), false);
                        newProject.IssueTypes.Add(new JiraVuIssueType(
                                ParentWindow.IssueCreationMetadata.GetIssueTypeForProject(projectKey, issueTypeName)));
                        ParentWindow.SelectedIssueCreationMeta.Projects.Add(newProject);
                    }
                    else
                    {   // Project already exists, check for existence of issue type
                        if (ParentWindow.SelectedIssueCreationMeta.GetIssueTypeForProject(projectKey, issueTypeName) == null)
                        {   // Add the issue issue type metadata for this project
                            ParentWindow.SelectedIssueCreationMeta.GetProject(projectKey).IssueTypes.Add(new JiraVuIssueType(
                                ParentWindow.IssueCreationMetadata.GetIssueTypeForProject(projectKey, issueTypeName)));
                        }
                    }
                }
            }
            ParentWindow.SelectedIssues = selectedIssues;
            return true;
        }

        public bool PrepMoveBack()
        {
            // Prepare for hand-off to the previous user control
            ((INavigationUserControl)BackUserControl).NextUserControl = this;
            return ((INavigationUserControl)BackUserControl).UpdateParentUI();
        }

        public bool PrepMoveNext()
        {
            // Prepare for hand-off to the next user control
            ((INavigationUserControl)NextUserControl).BackUserControl = this;
            return ((INavigationUserControl)NextUserControl).UpdateParentUI();
        }

        public bool UpdateParentUI()
        {
            ParentWindow.BackButton.IsEnabled = true;
            ParentWindow.NextButton.IsEnabled = true;
            return true;
        }

        #endregion

        #region Private Methods

        private void PerformSearch()
        {
            if (txtJql.Text.Trim() == Properties.Settings.Default.DefaultJqlMessage || txtJql.Text.Trim() == String.Empty) { return; }

            // Save current jql search
            ParentWindow.CurrentJqlSearch = txtJql.Text.Trim();

            try
            {
                BackgroundWorker worker = new BackgroundWorker();
                worker.WorkerSupportsCancellation = true;
                worker.DoWork += (o, ea) =>
                {
                    List<JiraIssue> jiraIssuesList = new List<JiraIssue>();
                    try
                    {
                        // Get the list of issues matching the jql
                        jiraIssuesList = ParentWindow.RestClient.GetIssuesByJqlSearch(
                            ParentWindow.CurrentJqlSearch, 0,
                            Properties.Settings.Default.MaxResultsInt);
                    }
                    catch (ArgumentException ae)
                    {   // Invalid JQL submitted
                        // Capture the error message provided by JIRA
                        ea.Result = ae.Message;
                        worker.CancelAsync();
                        return;
                    }

                    if (!worker.CancellationPending && jiraIssuesList.Count > 0)
                    {
                        // Convert JiraIssue list into JiraVuIssue list
                        jiraVuIssues = jiraIssuesList.ConvertAll<JiraVuIssue>(
                            delegate(JiraIssue i)
                            {
                                return new JiraVuIssue(i, ParentWindow.RestClient.ServerUrl);
                            });
                    }
                };
                worker.RunWorkerCompleted += (o, ea) =>
                {
                    // Note: You MUST check ea.Error and ea.Cancelled before you can get to ea.Result
                    if (ea.Error != null)
                    {
                        ParentWindow.ShowUserNotification(ea.Error.Message);
                    }

                    if (ea.Cancelled) { }

                    if (ea.Result != null)
                    {
                        ParentWindow.ShowUserNotification(ea.Result.ToString());
                    }

                    dgSearchResults.ItemsSource = jiraVuIssues;
                    tbRowCount.Text = dgSearchResults.Items.Count.ToString();
                    chkHeader.IsChecked = false;
                    busyIndicator1.IsBusy = false;
                    txtJql.Focus();
                };
                busyIndicator1.IsBusy = true;
                tbRowCount.Text = "...";
                ParentWindow.HideUserNotification();
                worker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                ParentWindow.ShowUserNotification(ex.Message);
            }
        }

        #endregion

        #region Event Handlers

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            PerformSearch();
        }

        private void chkHeader_Checked(object sender, RoutedEventArgs e)
        {
            foreach (JiraVuIssue issue in jiraVuIssues) { issue.IsSelected = true; }
        }

        private void chkHeader_Unchecked(object sender, RoutedEventArgs e)
        {
            foreach (JiraVuIssue issue in jiraVuIssues) { issue.IsSelected = false; }
        }

        private void chkItem_Checked(object sender, RoutedEventArgs e)
        {
            if (((CheckBox)sender).IsFocused)
            {
                ((JiraVuIssue)dgSearchResults.SelectedItem).IsSelected = true;
            }
        }

        private void chkItem_Unchecked(object sender, RoutedEventArgs e)
        {
            if (((CheckBox)sender).IsFocused)
            {
                ((JiraVuIssue)dgSearchResults.SelectedItem).IsSelected = false;
            }
        }

        private void txtJql_GotFocus(object sender, RoutedEventArgs e)
        {
            if (txtJql.Text.Trim() == Properties.Settings.Default.DefaultJqlMessage)
            {
                txtJql.Text = String.Empty;
            }
        }

        private void txtJql_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                PerformSearch();
            }
        }

        private void txtJql_LostFocus(object sender, RoutedEventArgs e)
        {
            if (txtJql.Text.Trim() == String.Empty)
            {
                txtJql.Text = Properties.Settings.Default.DefaultJqlMessage;
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            txtJql.Focus();
        }

        #endregion

    }
}
