﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Xml.Linq;
using System.Xml;
using System.Data;
using System.Windows.Threading;
using JiraRestClient;
using JiraVu.Core;

namespace JiraVu.Views
{
    /// <summary>
    /// Interaction logic for IssueTreeViewUserControl.xaml
    /// </summary>
    public partial class IssueTreeViewUserControl : UserControl, INavigationUserControl, INotifyPropertyChanged
    {
        private bool fileHasChanges;
        private FilterFolder selectedFilterFolder;
        private JiraVuIssue selectedJiraVuIssue;
        private List<FilterFolder> rootFolders;
        private string lastFileName = String.Empty;
        private string _searchText = String.Empty;
        private UserControl _ucBackControl;
        private UserControl _ucNextControl;
        private Visibility dataGridVisibility = Visibility.Collapsed;
        private XmlWriter writer;
        private DispatcherTimer autoRefreshTimer;

        #region Commands

        private Command<FilterFolder> refreshFolderCommand;
        public Command<FilterFolder> RefreshFolderCommand
        {
            get
            {
                if (refreshFolderCommand == null)
                    refreshFolderCommand = new Command<FilterFolder>(RefreshFolder);
                return refreshFolderCommand;
            }
        }

        private Command<FilterFolder> searchFolderCommand;
        public Command<FilterFolder> SearchFolderCommand
        {
            get
            {
                if (searchFolderCommand == null)
                    searchFolderCommand = new Command<FilterFolder>(PerformSearch);
                return searchFolderCommand;
            }
        }

        #endregion Commands

        #region Public Properties

        public bool FileHasChanges
        {
            get { return fileHasChanges; }
            set { fileHasChanges = value; RaisePropertyChanged("FileHasChanges"); }
        }

        public FilterFolder SelectedFilterFolder
        {
            get { return selectedFilterFolder; }
            set { selectedFilterFolder = value; RaisePropertyChanged("SelectedFilterFolder"); }
        }

        public JiraVuIssue SelectedJiraVuIssue
        {
            get { return selectedJiraVuIssue; }
            set { selectedJiraVuIssue = value; RaisePropertyChanged("SelectedJiraVuIssue"); }
        }

        public List<FilterFolder> RootFolders
        {
            get { return rootFolders; }
            set { 
                rootFolders = value;
                RaisePropertyChanged("RootFolders");
            }
        }

        public TabItem ParentTab { get; set; }

        public MainWindow ParentWindow { get; set; }

        public string CurrentFile
        {
            get
            {
                return lastFileName;
            }
            set
            {
                lastFileName = value; RaisePropertyChanged("CurrentFile");
            }
        }

        public string SearchText
        {
            get { return _searchText; }
            set
            {
                if (value == _searchText)
                    return;

                _searchText = value;

                _matchingNodeEnumerator = null;
                ParentWindow.HideUserNotification();
            }
        }

        public UserControl BackUserControl
        {
            get
            {
                if (_ucBackControl == null)
                    _ucBackControl = new MainMenuUserControl(ParentWindow);
                return _ucBackControl;
            }
            set
            {
                _ucBackControl = value;
            }
        }

        public UserControl NextUserControl
        {
            get
            {   
                if (_ucNextControl == null) 
                    _ucNextControl = new MainMenuUserControl(ParentWindow);
                return _ucNextControl;
            }
            set
            {
                _ucNextControl = value;
            }
        }

        public Visibility DataGridVisibility
        {
            get { return dataGridVisibility; }
            set { dataGridVisibility = value; RaisePropertyChanged("DataGridVisibility"); }
        }

        #endregion Public Properties

        #region Constructors

        public IssueTreeViewUserControl(MainWindow parentWindow)
        {
            InitializeComponent();
            ParentWindow = parentWindow;
            DataContext = this;

            AddTreeViewRootNode();

            InitializeAutoRefreshTimer();
        }

        public IssueTreeViewUserControl(MainWindow parentWindow, TabItem parentTab)
            : this(parentWindow)
        {
            ParentTab = parentTab;
        }

        #endregion Constructors

        #region Visual Tree Helper

        public static T VisualUpwardSearch<T>(DependencyObject source) where T : DependencyObject
        {
            DependencyObject returnVal = source;

            while (returnVal != null && !(returnVal is T))
            {
                DependencyObject tempReturnVal = null;
                if (returnVal is Visual || returnVal is System.Windows.Media.Media3D.Visual3D)
                {
                    tempReturnVal = VisualTreeHelper.GetParent(returnVal);
                }
                if (tempReturnVal == null)
                {
                    returnVal = LogicalTreeHelper.GetParent(returnVal);
                }
                else returnVal = tempReturnVal;
            }

            return returnVal as T;
        }

        #endregion Visual Tree Helper

        #region Public Methods

        public bool DoWorkAsync()
        {
            return true;
        }

        public void InitializeAutoRefreshTimer()
        {
            //autoRefreshTimer = new DispatcherTimer();
            //autoRefreshTimer.Interval = TimeSpan.FromMinutes(autoRefreshSlider.Value);
            //autoRefreshTimer.Tick += autoRefreshTimer_Tick;
        }

        public void LoadTreeViewFromFile()
        {
            if (PromptForSave() == MessageBoxResult.Cancel) return;

            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.Filter = "XML Files (*.xml)|*.xml";
            dlg.DefaultExt = ".xml";
            bool? result = dlg.ShowDialog(ParentWindow);
            if (result == false) return;

            string fileName = dlg.FileName;

            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(fileName);

                // Build the root node from the root element
                FilterFolder rootFolder = new FilterFolder(
                    doc.DocumentElement.Attributes["name"].Value,
                    null,
                    doc.DocumentElement.Attributes["jql"].Value);

                rootFolder.IsExpanded = true;
                rootFolder.Children = new ObservableCollection<INode>();

                RootFolders = new List<FilterFolder>() { rootFolder };

                // Add child nodes to tree view
                foreach (XmlNode node in doc.DocumentElement.ChildNodes)
                    LoadTreeViewItem(rootFolder, node);

                lastFileName = fileName;
                //TODO: Replace with binding
                int index1 = fileName.LastIndexOf('\\') + 1;
                int index2 = fileName.LastIndexOf('.');
                ParentTab.Header = fileName.Substring(index1, index2 - index1);
            }
            catch (Exception ex) { ParentWindow.ShowUserNotification(ex.Message); }
        }

        public bool PrepMoveBack()
        {
            // Prepare for hand-off to the previous user control
            ((INavigationUserControl)BackUserControl).NextUserControl = this;
            if (PromptForSave() == MessageBoxResult.Cancel) return false;
            return ((INavigationUserControl)BackUserControl).UpdateParentUI();
        }

        public bool PrepMoveNext()
        {
            // Prepare for hand-off to the next user control
            ((INavigationUserControl)NextUserControl).BackUserControl = this;
            if (PromptForSave() == MessageBoxResult.Cancel) return false;
            return ((INavigationUserControl)NextUserControl).UpdateParentUI();
        }

        public MessageBoxResult PromptForSave()
        {
            if (!FileHasChanges) return MessageBoxResult.None;

            MessageBoxResult result = MessageBox.Show(ParentWindow,
                "Would you like to save your changes?", "Save Changes?",
                MessageBoxButton.YesNoCancel, MessageBoxImage.Question);

            if (result == MessageBoxResult.Yes)
            {
                SaveTreeViewToFile();
            }

            return result;
        }

        public void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public void RefreshFolder(FilterFolder f)
        {
            if (f == null) return;
            if (f.JiraJql.Trim() == Properties.Settings.Default.DefaultJqlMessage) { return; }

            List<JiraIssue> jiraIssues = new List<JiraIssue>();

            try
            {
                BackgroundWorker worker = new BackgroundWorker();
                worker.WorkerSupportsCancellation = true;
                worker.DoWork += (o, ea) =>
                {
                    try
                    {
                        // Get the list of issues matching the jql
                        jiraIssues = ParentWindow.RestClient.GetIssuesByJqlSearch(
                            f.JiraJql, 0, Properties.Settings.Default.MaxResultsInt);
                    }
                    catch (ArgumentException ae)
                    {   // Invalid JQL submitted
                        // Capture the error message provided by JIRA
                        ea.Result = ae.Message;
                        worker.CancelAsync();
                        return;
                    }
                    catch (Exception ex)
                    {
                        ea.Result = ex.Message;
                        worker.CancelAsync();
                        return;
                    }
                };
                worker.RunWorkerCompleted += (o, ea) =>
                {
                    // Note: You MUST check ea.Error and ea.Cancelled before you can get to ea.Result
                    if (ea.Error != null)
                    {
                        ParentWindow.ShowUserNotification(ea.Error.Message);
                    }

                    if (ea.Cancelled) { }

                    if (ea.Result != null)
                    {
                        ParentWindow.ShowUserNotification(ea.Result.ToString());
                    }

                    // Find all child issues for this folder...
                    var issuesToRemove = f.Children.Where(i => i is JiraVuIssue).ToList();
                    // ...and remove them
                    foreach (JiraVuIssue jvi in issuesToRemove)
                        f.Children.Remove(jvi);
                    
                    // Add the children to the current node, remove duplicates from parent
                    foreach (JiraIssue iss in jiraIssues)
                    {   // Convert JiraIssue list into JiraVuIssue list
                        JiraVuIssue ji = new JiraVuIssue(iss, ParentWindow.RestClient.ServerUrl, f);
                        //TODO: Remove the following lines; they shouldn't be necessary
                        ObservableCollection<JiraIssue> temp = ji.LinkedIssues;
                        ObservableCollection<LinkedIssuesButton> temp2 = ji.LinkedIssuesButtons;

                        // Add issue to the folder
                        f.Children.Add(ji);
                        f.RaisePropertyChanged("Children");
                        f.RaisePropertyChanged("ChildrenStatusCollection");

                        // Remove issue from parent folder if it exists there
                        if (f.ParentFolder != null)
                        {
                            if (f.ParentFolder.Children.Contains(ji))
                                f.ParentFolder.Children.Remove(ji);
                        }
                    }

                    // Update the direct issue count for this folder
                    f.UpdateChildIssueCount();

                    // Propagate issue count updates up the tree
                    FilterFolder parent = f.ParentFolder;
                    while (parent != null)
                    {
                        parent.UpdateChildIssueCount();
                        parent = parent.ParentFolder;
                    }

                    if (parent == null)
                    {   // Root node was refreshed, update last refresh info
                        tbLastRefreshLabel.Visibility = System.Windows.Visibility.Visible;
                        tbLastRefreshTime.Visibility = System.Windows.Visibility.Visible;
                        tbLastRefreshTime.Text = DateTime.Now.ToString();
                    }

                    // Refresh all child filter folders
                    foreach (INode childNode in f.Children)
                    {
                        if (childNode is FilterFolder)
                            RefreshFolder(childNode as FilterFolder);
                    }

                    busyIndicator1.IsBusy = false;
                };

                busyIndicator1.IsBusy = true;
                ParentWindow.HideUserNotification();
                worker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                ParentWindow.ShowUserNotification(ex.Message);
            }
        }

        public void ResetTreeView()
        {
            if (PromptForSave() == MessageBoxResult.Cancel) return;
            AddTreeViewRootNode();
            ParentWindow.WindowTitle = String.Empty;
            lastFileName = String.Empty;
            DataGridVisibility = System.Windows.Visibility.Collapsed;
        }

        public void SaveTreeViewToFile()
        {
            if (lastFileName == String.Empty)
            {
                SaveTreeViewToFileAs();
                return;
            }

            try
            {
                XmlWriterSettings settings = new XmlWriterSettings() { Indent = true };
                writer = XmlWriter.Create(lastFileName, settings);
                writer.WriteStartDocument();
                writer.WriteStartElement("FilterFolder");
                writer.WriteAttributeString("name", RootFolders[0].Name);
                writer.WriteAttributeString("jql", RootFolders[0].JiraJql);

                foreach (INode node in RootFolders[0].Children)
                {
                    WriteTreeViewItemToXml(node);
                }

                writer.WriteEndElement();
                writer.WriteEndDocument();

                writer.Flush();
                FileHasChanges = false;
            }
            catch (Exception ex) { ParentWindow.ShowUserNotification(ex.Message); }
            finally
            {
                writer.Close();
            }
        }

        public void SaveTreeViewToFileAs()
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.Filter = "XML Files (*.xml)|*.xml";
            dlg.DefaultExt = ".xml";
            bool? result = dlg.ShowDialog(ParentWindow);
            if (result == false) return;

            string fileName = dlg.FileName;

            try
            {
                XmlWriterSettings settings = new XmlWriterSettings() { Indent = true };
                writer = XmlWriter.Create(fileName, settings);
                writer.WriteStartDocument();
                writer.WriteStartElement("FilterFolder");
                writer.WriteAttributeString("name", RootFolders[0].Name);
                writer.WriteAttributeString("jql", RootFolders[0].JiraJql);

                foreach (INode node in RootFolders[0].Children)
                {
                    WriteTreeViewItemToXml(node);
                }

                writer.WriteEndElement();
                writer.WriteEndDocument();

                writer.Flush();
                FileHasChanges = false;
            }
            catch (Exception ex) { ParentWindow.ShowUserNotification(ex.Message); }
            finally
            {
                writer.Close();
            }
        }

        public void StartAutoRefreshTimer()
        {
            if (autoRefreshSlider == null) return;
            if (autoRefreshTimer == null)
                InitializeAutoRefreshTimer();

            autoRefreshTimer.Start();
        }

        public void StopAutoRefreshTimer()
        {
            if (autoRefreshSlider == null) return;

            autoRefreshTimer.Stop();
            autoRefreshTimer = null;
        }
        
        public bool UpdateParentUI()
        {
            ParentWindow.BackButton.IsEnabled = true;
            ParentWindow.NextButton.IsEnabled = true;
            return true;
        }

        #endregion

        #region Search Logic

        private IEnumerator<INode> _matchingNodeEnumerator;

        public void PerformSearch(FilterFolder folderToSearch)
        {
            if (_matchingNodeEnumerator == null || !_matchingNodeEnumerator.MoveNext())
                this.VerifyMatchingNodeEnumerator(folderToSearch);

            var node = _matchingNodeEnumerator.Current;

            if (node == null) return;

            FilterFolder parentFolder = node.ParentFolder;
            while (parentFolder != null)
            {
                parentFolder.IsExpanded = true;
                parentFolder = parentFolder.ParentFolder;
            }
            
            node.IsSelected = true;
        }

        private void VerifyMatchingNodeEnumerator(FilterFolder folderToSearch)
        {
            var matches = this.FindMatches(_searchText, folderToSearch);
            _matchingNodeEnumerator = matches.GetEnumerator();

            if (!_matchingNodeEnumerator.MoveNext())
            {
                ParentWindow.ShowUserNotification("No matches were found.");
            }
        }

        private IEnumerable<INode> FindMatches(string searchText, INode node)
        {
            if (node is FilterFolder)
            {
                FilterFolder f = node as FilterFolder;
                if (StringContains(f.Name, searchText))
                    yield return node;

                foreach (INode child in f.Children)
                    foreach (INode match in this.FindMatches(searchText, child))
                        yield return match;
            }
            else if (node is JiraVuIssue)
            {
                JiraVuIssue i = node as JiraVuIssue;
                if (StringContains(i.JiraIssue.key, searchText) || StringContains(i.JiraIssue.fields.summary, searchText))
                    yield return node;
            }

        }

        #endregion Search Logic

        #region Private Methods

        private void AddTreeViewRootNode()
        {
            FilterFolder rootFolder = new FilterFolder("Root");
            rootFolder.HasParent = false;
            rootFolder.IsExpanded = true;
            rootFolder.Children = new ObservableCollection<INode>();

            RootFolders = new List<FilterFolder>() { rootFolder };

            SelectedFilterFolder = RootFolders[0];
        }

        private void LoadTreeViewItem(INode parentNode, XmlNode xmlNode)
        {
            if (parentNode is FilterFolder)
            {
                FilterFolder fldr = new FilterFolder(
                    xmlNode.Attributes["name"].Value,
                    parentNode as FilterFolder,
                    xmlNode.Attributes["jql"].Value);

                ((FilterFolder)parentNode).Children.Add(fldr);

                foreach (XmlNode node in xmlNode.ChildNodes)
                    LoadTreeViewItem(fldr, node);
            }
        }

        private void SetTreeViewItemsIsExpanded(FilterFolder f, bool isExpanded)
        {
            f.IsExpanded = isExpanded;
            foreach (INode node in f.Children)
            {
                if (node is FilterFolder)
                {
                    SetTreeViewItemsIsExpanded(node as FilterFolder, isExpanded);
                }
            }
        }

        private bool StringContains(string toSearch, string criteria)
        {
            if (String.IsNullOrEmpty(toSearch) || String.IsNullOrEmpty(criteria)) return false;
            return toSearch.IndexOf(criteria, StringComparison.InvariantCultureIgnoreCase) > -1;
        }

        private void WriteTreeViewItemToXml(INode node)
        {
            if (node is FilterFolder)
            {
                FilterFolder fldr = node as FilterFolder;
                writer.WriteStartElement("FilterFolder");
                writer.WriteAttributeString("name", fldr.Name);
                writer.WriteAttributeString("jql", fldr.JiraJql);

                foreach (INode childNode in fldr.Children)
                {
                    WriteTreeViewItemToXml(childNode);
                }

                writer.WriteEndElement();
            }
        }

        #endregion Private Methods
        
        #region Event Handlers

        public event PropertyChangedEventHandler PropertyChanged;

        private void autoRefreshSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            InitializeAutoRefreshTimer();

            if (autoRefreshSlider.IsFocused == true)
                StartAutoRefreshTimer();
        }

        private void autoRefreshTimer_Tick(object sender, EventArgs e)
        {
            RefreshFolder(RootFolders[0]);
        }

        private void dgLinkedIssues_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                int clmnIndex = ((DataGrid)sender).SelectedCells[0].Column.DisplayIndex;
                DataGrid grd = sender as DataGrid;
                JiraIssue iss = grd.SelectedCells[0].Item as JiraIssue;

                Hyperlink link = new Hyperlink();
                link.NavigateUri = new Uri(String.Format("{0}/browse/{1}", ParentWindow.RestClient.ServerUrl, iss.key));
                System.Diagnostics.Process.Start(link.NavigateUri.AbsoluteUri);
            }
            catch (Exception ex) { }
        }

        private void LinkedIssuesButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //LinkedIssuesButton btn = e.OriginalSource as LinkedIssuesButton;
            }
            catch (Exception ex) { ParentWindow.ShowUserNotification(ex.Message); }
        }

        private void menuAutoRefresh_Check(object sender, RoutedEventArgs e)
        {
            StartAutoRefreshTimer();
        }

        private void menuAutoRefresh_Uncheck(object sender, RoutedEventArgs e)
        {
            StopAutoRefreshTimer();
        }

        private void menuCollapseAll_Click(object sender, RoutedEventArgs e)
        {
            TreeViewItem treeViewItem =
                VisualUpwardSearch<TreeViewItem>(e.OriginalSource as DependencyObject);
            if (treeViewItem != null)
            {
                SetTreeViewItemsIsExpanded(treeViewItem.Header as FilterFolder, false);
            }
        }

        private void menuDelete_Click(object sender, RoutedEventArgs e)
        {
            TreeViewItem treeViewItem =
                VisualUpwardSearch<TreeViewItem>(e.OriginalSource as DependencyObject);
            if (treeViewItem != null)
            {
                if (treeViewItem.Header is FilterFolder)
                {
                    FilterFolder fldr = (FilterFolder)treeViewItem.Header;
                    fldr.ParentFolder.DeleteChild(fldr);
                }
            }
        }

        private void menuExpandAll_Click(object sender, RoutedEventArgs e)
        {
            TreeViewItem treeViewItem =
                VisualUpwardSearch<TreeViewItem>(e.OriginalSource as DependencyObject);
            if (treeViewItem != null)
            {
                SetTreeViewItemsIsExpanded(treeViewItem.Header as FilterFolder, true);
            }
        }

        private void menuLoadFromFile_Click(object sender, RoutedEventArgs e)
        {
            LoadTreeViewFromFile();
        }

        private void menuMoveDown_Click(object sender, RoutedEventArgs e)
        {
            TreeViewItem treeViewItem =
                VisualUpwardSearch<TreeViewItem>(e.OriginalSource as DependencyObject);
            if (treeViewItem != null)
            {
                if (treeViewItem.Header is FilterFolder)
                {
                    FilterFolder fldr = (FilterFolder)treeViewItem.Header;
                    fldr.ParentFolder.MoveChildDown(fldr);
                    FileHasChanges = true;
                }
            }
        }

        private void menuMoveUp_Click(object sender, RoutedEventArgs e)
        {
            TreeViewItem treeViewItem =
                VisualUpwardSearch<TreeViewItem>(e.OriginalSource as DependencyObject);
            if (treeViewItem != null)
            {
                if (treeViewItem.Header is FilterFolder)
                {
                    FilterFolder fldr = (FilterFolder)treeViewItem.Header;
                    fldr.ParentFolder.MoveChildUp(fldr);
                    FileHasChanges = true;
                }
            }
        }

        private void menuNewTree_Click(object sender, RoutedEventArgs e)
        {
            ResetTreeView();
        }

        private void menuRefresh_Click(object sender, RoutedEventArgs e)
        {
            TreeViewItem treeViewItem = 
                VisualUpwardSearch<TreeViewItem>(e.OriginalSource as DependencyObject);
            if (treeViewItem != null)
            {
                if (treeViewItem.Header is FilterFolder)
                    RefreshFolder(treeViewItem.Header as FilterFolder);
            }
        }

        private void menuSaveFile_Click(object sender, RoutedEventArgs e)
        {
            SaveTreeViewToFile();
        }

        private void menuSaveAsFile_Click(object sender, RoutedEventArgs e)
        {
            SaveTreeViewToFileAs();
        }

        private void miInsertNewFilterFolderAbove_Click(object sender, RoutedEventArgs e)
        {
            FilterFolder currFldr = SelectedFilterFolder;
            FilterFolder newFldr = new FilterFolder("New Filter Folder", currFldr.ParentFolder);
            newFldr.JiraJql = currFldr.JiraJql;
            currFldr.ParentFolder.Children.Insert(currFldr.ParentFolder.Children.IndexOf(currFldr), newFldr);
        }

        private void miInsertNewFilterFolderBelow_Click(object sender, RoutedEventArgs e)
        {
            FilterFolder currFldr = SelectedFilterFolder;
            FilterFolder newFldr = new FilterFolder("New Filter Folder", currFldr.ParentFolder);
            newFldr.JiraJql = currFldr.JiraJql;
            currFldr.ParentFolder.Children.Insert(currFldr.ParentFolder.Children.IndexOf(currFldr) + 1, newFldr);
        }

        private void miNewChildFilterFolderEmptyJql_Click(object sender, RoutedEventArgs e)
        {
            FilterFolder currFldr = SelectedFilterFolder;
            FilterFolder newFldr = new FilterFolder("New Filter Folder", currFldr.ParentFolder);
            currFldr.Children.Insert(0, newFldr);
        }

        private void miNewChildFilterFolder_Click(object sender, RoutedEventArgs e)
        {
            FilterFolder currFldr = SelectedFilterFolder;
            FilterFolder newFldr = new FilterFolder("New Filter Folder", currFldr);
            newFldr.JiraJql = currFldr.JiraJql;
            currFldr.Children.Insert(0, newFldr);
        }

        private void miNewChildFoldersLinkedIssues_Click(object sender, RoutedEventArgs e)
        {
            FilterFolder currFldr = SelectedFilterFolder;
            FilterFolder parFldr = currFldr.ParentFolder;

            // Find all child issues for this folder...
            var issues = SelectedFilterFolder.Children.Where(i => i is JiraVuIssue).ToList();
            // ...and create new filter folders to contain their linked issues
            foreach (JiraVuIssue ji in issues)
            {
                FilterFolder newFldr = new FilterFolder(
                    String.Format("Issues linked to {0}", ji.JiraIssue.key),
                    currFldr);
                newFldr.JiraJql = String.Format("issue in linkedIssues(\"{0}\")", ji.JiraIssue.key);
                currFldr.Children.Insert(0, newFldr);
            }
        }

        private void TreeViewItem_PreviewMouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            TreeViewItem treeViewItem =
              VisualUpwardSearch<TreeViewItem>(e.OriginalSource as DependencyObject);

            if (treeViewItem != null)
            {
                if (treeViewItem.Header is FilterFolder)
                {
                    FilterFolder fldr = (FilterFolder)treeViewItem.Header;
                    SelectedFilterFolder = fldr;

                    ContextMenu cntxtMenu = new System.Windows.Controls.ContextMenu();

                    MenuItem miNewChildFilterFolder = new MenuItem();
                    miNewChildFilterFolder.Header = "New Child Filter Folder";
                    miNewChildFilterFolder.Click += miNewChildFilterFolder_Click;

                    MenuItem miNewChildFilterFolderEmptyJql = new MenuItem();
                    miNewChildFilterFolderEmptyJql.Header = "New Child Filter Folder - Empty JQL";
                    miNewChildFilterFolderEmptyJql.Click += miNewChildFilterFolderEmptyJql_Click;

                    MenuItem miInsertNewFilterFolderAbove = new MenuItem();
                    miInsertNewFilterFolderAbove.Header = "Insert New Filter Folder Above";
                    miInsertNewFilterFolderAbove.Click += miInsertNewFilterFolderAbove_Click;

                    MenuItem miInsertNewFilterFolderBelow = new MenuItem();
                    miInsertNewFilterFolderBelow.Header = "Insert New Filter Folder Below";
                    miInsertNewFilterFolderBelow.Click += miInsertNewFilterFolderBelow_Click;

                    MenuItem miNewChildFoldersLinkedIssues = new MenuItem();
                    miNewChildFoldersLinkedIssues.Header = "New Child Folders for Linked Issues";
                    miNewChildFoldersLinkedIssues.Click += miNewChildFoldersLinkedIssues_Click;

                    cntxtMenu.Items.Add(miNewChildFilterFolder);
                    cntxtMenu.Items.Add(miNewChildFilterFolderEmptyJql);
                    cntxtMenu.Items.Add(new Separator());
                    cntxtMenu.Items.Add(miInsertNewFilterFolderAbove);
                    cntxtMenu.Items.Add(miInsertNewFilterFolderBelow);
                    cntxtMenu.Items.Add(new Separator());
                    cntxtMenu.Items.Add(miNewChildFoldersLinkedIssues);

                    treeViewItem.ContextMenu = cntxtMenu;

                    FileHasChanges = true;
                }

                e.Handled = true;
            }
        }

        private void tvIssues_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F && Keyboard.Modifiers.HasFlag(ModifierKeys.Control))
            {
                txtSearchTree.Focus();
                txtSearchTree.SelectAll();
            }
        }

        private void tvIssues_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Hyperlink link = new Hyperlink();
            try
            {
                INode selectedItem = ((TreeView)sender).SelectedItem as INode;
                if (selectedItem is JiraVuIssue)
                {
                    link.NavigateUri = ((JiraVuIssue)((TreeView)sender).SelectedItem).IssueUri;
                    System.Diagnostics.Process.Start(link.NavigateUri.AbsoluteUri);
                }
            }
            catch (Exception ex) { }
        }

        private void tvIssues_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (e.NewValue is FilterFolder)
            {
                SelectedFilterFolder = (FilterFolder)e.NewValue;
                SelectedJiraVuIssue = null;
                DataGridVisibility = System.Windows.Visibility.Collapsed;
            }
            else if (e.NewValue is JiraVuIssue)
            {
                SelectedJiraVuIssue = (JiraVuIssue)e.NewValue;
                DataGridVisibility = System.Windows.Visibility.Visible;
            }
        }

        private void txtJql_GotFocus(object sender, RoutedEventArgs e)
        {
            if (txtJql.Text.Trim() == Properties.Settings.Default.DefaultJqlMessage)
            {
                txtJql.SelectAll();
            }
        }

        private void txtJql_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                SelectedFilterFolder.JiraJql = txtJql.Text.Trim();
                RefreshFolder(SelectedFilterFolder);
            }
        }

        private void txtSearchTree_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                _searchText = txtSearchTree.Text.Trim();
                ParentWindow.HideUserNotification();
                SearchFolderCommand.Execute(SelectedFilterFolder);
            }
        }

        #endregion Event Handlers

    }

    public static class TreeViewItemBehavior
    {
        public static bool GetIsBroughtIntoViewWhenSelected(TreeViewItem treeViewItem)
        {
            return (bool)treeViewItem.GetValue(IsBroughtIntoViewWhenSelectedProperty);
        }

        public static void SetIsBroughtIntoViewWhenSelected(
          TreeViewItem treeViewItem, bool value)
        {
            treeViewItem.SetValue(IsBroughtIntoViewWhenSelectedProperty, value);
        }

        public static readonly DependencyProperty IsBroughtIntoViewWhenSelectedProperty =
            DependencyProperty.RegisterAttached(
                "IsBroughtIntoViewWhenSelected",
                typeof(bool),
                typeof(TreeViewItemBehavior),
                new UIPropertyMetadata(false, OnIsBroughtIntoViewWhenSelectedChanged));

        static void OnIsBroughtIntoViewWhenSelectedChanged(
          DependencyObject depObj, DependencyPropertyChangedEventArgs e)
        {
            TreeViewItem item = depObj as TreeViewItem;
            if (item == null)
                return;

            if (e.NewValue is bool == false)
                return;

            if ((bool)e.NewValue)
                item.Selected += OnTreeViewItemSelected;
            else
                item.Selected -= OnTreeViewItemSelected;
        }

        static void OnTreeViewItemSelected(object sender, RoutedEventArgs e)
        {
            // Only react to the Selected event raised by the TreeViewItem
            // whose IsSelected property was modified. Ignore all ancestors
            // who are merely reporting that a descendant's Selected fired.
            if (!Object.ReferenceEquals(sender, e.OriginalSource))
                return;

            TreeViewItem item = e.OriginalSource as TreeViewItem;
            if (item != null)
                item.BringIntoView();
        }
    }
}
