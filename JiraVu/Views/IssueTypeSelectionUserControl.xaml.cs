﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using JiraRestClient;
using JiraVu.Core;

namespace JiraVu.Views
{
    /// <summary>
    /// Interaction logic for IssueTypeSelectionUserControl.xaml
    /// </summary>
    public partial class IssueTypeSelectionUserControl : UserControl, INavigationUserControl
    {
        private UserControl _ucBackControl;
        private UserControl _ucNextControl;

        #region Public Properties

        public JiraVuIssueType.IssueTypeClasses TypesToDisplay { get; set; }
        public List<JiraVuIssueType> IssueTypes { get; set; }
        public MainWindow ParentWindow { get; set; }
        public UserControl BackUserControl
        {
            get
            {
                return _ucBackControl;
            }
            set
            {
                _ucBackControl = value;
            }
        }
        public UserControl NextUserControl
        {
            get
            {
                return _ucNextControl;
            }
            set
            {
                _ucNextControl = value;
            }
        }

        #endregion

        #region Constructors

        public IssueTypeSelectionUserControl(MainWindow parentWindow, JiraVuIssueType.IssueTypeClasses typesToDisplay)
        {
            InitializeComponent();
            this.DataContext = this;
            this.ParentWindow = parentWindow;
            this.TypesToDisplay = typesToDisplay;
        }

        #endregion

        #region Public Methods

        public bool DoWorkAsync()
        {
            ParentWindow.SelectedIssueType = (JiraVuIssueType)lbIssueTypes.SelectedItem;
            return true;
        }

        public bool PrepMoveBack()
        {
            // Prepare for hand-off to the previous user control
            ((INavigationUserControl)BackUserControl).NextUserControl = this;
            return ((INavigationUserControl)BackUserControl).UpdateParentUI();
        }

        public bool PrepMoveNext()
        {
            // Prepare for hand-off to the next user control
            ((INavigationUserControl)NextUserControl).BackUserControl = this;
            return ((INavigationUserControl)NextUserControl).UpdateParentUI();
        }

        public bool UpdateParentUI()
        {
            ParentWindow.BackButton.IsEnabled = true;
            ParentWindow.NextButton.IsEnabled = false;
            return true;
        }

        #endregion

        #region Event Handlers

        private void lbIssueTypes_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ParentWindow.MoveNext();
        }
        
        private void lbIssueTypes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ParentWindow.NextButton.IsEnabled = true;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            List<JiraVuIssueType> issueTypes = ParentWindow.GetDistinctIssueTypeMeta();
            this.IssueTypes = new List<JiraVuIssueType>();

            switch (this.TypesToDisplay)
            {
                case JiraVuIssueType.IssueTypeClasses.All:
                    {
                        foreach (JiraVuIssueType type in issueTypes)
                        {
                            this.IssueTypes.Add(type);
                        }
                        break;
                    }
                case JiraVuIssueType.IssueTypeClasses.Standard:
                    {
                        foreach (JiraVuIssueType type in issueTypes)
                        {
                            if (!type.JiraIssueType.subtask) this.IssueTypes.Add(type);
                        }
                        break;
                    }
                case JiraVuIssueType.IssueTypeClasses.Subtask:
                    {
                        foreach (JiraVuIssueType type in issueTypes)
                        {
                            if (type.JiraIssueType.subtask) this.IssueTypes.Add(type);
                        }
                        break;
                    }
            }

            lbIssueTypes.ItemsSource = this.IssueTypes;
        }

        #endregion

    }
}
