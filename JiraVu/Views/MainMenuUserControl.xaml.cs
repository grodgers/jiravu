﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using JiraVu.Core;

namespace JiraVu.Views
{
    /// <summary>
    /// Interaction logic for MainMenuUserControl.xaml
    /// </summary>
    public partial class MainMenuUserControl : UserControl, INavigationUserControl
    {
        private UserControl _ucBackControl;
        private UserControl _ucNextControl;

        public MainWindow ParentWindow { get; set; }

        public MainMenuUserControl(MainWindow parentWindow)
        {
            InitializeComponent();
            ParentWindow = parentWindow;
        }

        private void btnIssueNavigator_Click(object sender, RoutedEventArgs e)
        {
            NextUserControl = new IssueNavigatorUserControl(ParentWindow);
            ParentWindow.MoveNext();
        }

        private void bnIssueTreeView_Click(object sender, RoutedEventArgs e)
        {
            NextUserControl = new IssueTreeViewUserControl(ParentWindow);
            ParentWindow.MoveNext();
        }

        public UserControl BackUserControl
        {
            get
            {
                if (_ucBackControl == null)
                    _ucBackControl = new JiraRestAuthUserControl(ParentWindow);
                return _ucBackControl;
            }
            set { _ucBackControl = value; }
        }

        public UserControl NextUserControl
        {
            get { return _ucNextControl; }
            set { _ucNextControl = value; }
        }

        public bool DoWorkAsync()
        {
            return true;
        }

        public bool PrepMoveNext()
        {
            ParentWindow.BackButton.IsEnabled = true;
            ParentWindow.NextButton.IsEnabled = true;
            ParentWindow.BackButton.Visibility = System.Windows.Visibility.Visible;
            ParentWindow.NextButton.Visibility = System.Windows.Visibility.Visible;
            return true;
        }

        public bool PrepMoveBack()
        {
            return true;
        }

        public bool UpdateParentUI()
        {
            ParentWindow.BackButton.Visibility = System.Windows.Visibility.Hidden;
            ParentWindow.NextButton.Visibility = System.Windows.Visibility.Hidden;
            return true;
        }
    }
}
