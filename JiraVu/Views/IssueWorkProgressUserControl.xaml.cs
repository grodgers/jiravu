﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using JiraRestClient;
using JiraVu.Core;

namespace JiraVu.Views
{
    /// <summary>
    /// Interaction logic for IssueWorkProgressUserControl.xaml
    /// </summary>
    public partial class IssueWorkProgressUserControl : UserControl, INavigationUserControl, INotifyPropertyChanged
    {
        public delegate void ProcessIssuesDelegate(JiraVuIssue issue);

        private List<JiraVuIssue> issuesToWorkOn;
        private string _strTitle;
        private UserControl _ucBackControl;
        private UserControl _ucNextControl;

        #region Public Properties

        public MainWindow ParentWindow { get; set; }

        public ProcessIssuesDelegate ProcessIssuesMethod { get; set; }

        public string Title
        {
            get { return _strTitle; }
            set
            {
                _strTitle = value;
                NotifyPropertyChanged("Title");
            }
        }

        public UserControl BackUserControl
        {
            get
            {
                return _ucBackControl;
            }
            set
            {
                _ucBackControl = value;
            }
        }

        public UserControl NextUserControl
        {
            get
            {
                if (_ucNextControl == null) _ucNextControl = new IssueNavigatorUserControl(this.ParentWindow);
                return _ucNextControl;
            }
            set
            {
                _ucNextControl = value;
            }
        }

        #endregion

        #region Constructors

        public IssueWorkProgressUserControl(MainWindow parentWindow, string title, ProcessIssuesDelegate processIssuesDelegate)
        {
            InitializeComponent();
            this.ParentWindow = parentWindow;
            this.DataContext = this;
            this.Title = title;
            this.ProcessIssuesMethod = processIssuesDelegate;
            issuesToWorkOn = ParentWindow.SelectedIssues;
            dgIssues.ItemsSource = issuesToWorkOn;
            tbRowCount.Text = issuesToWorkOn.Count.ToString();
        }

        #endregion

        #region Public Methods
        
        public bool DoWorkAsync()
        {
            return true;
        }

        public bool PrepMoveBack()
        {
            // Prepare for hand-off to the previous user control
            ((INavigationUserControl)BackUserControl).NextUserControl = this;
            return ((INavigationUserControl)BackUserControl).UpdateParentUI();
        }

        public bool PrepMoveNext()
        {
            // Prepare for hand-off to the next user control
            ((INavigationUserControl)NextUserControl).BackUserControl = this;
            return ((INavigationUserControl)NextUserControl).UpdateParentUI();
        }

        public bool UpdateParentUI()
        {
            ParentWindow.BackButton.IsEnabled = true;
            ParentWindow.NextButton.IsEnabled = true;
            return true;
        }

        #endregion

        #region Private Methods
        
        /// <summary>
        /// Notify clients, typically binding clients, that a property value has changed.
        /// </summary>
        /// <param name="info">The property that has changed</param>
        private void NotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(info));
        }

        #endregion

        #region Event Handlers

        public event PropertyChangedEventHandler PropertyChanged;

        private void btnBegin_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                BackgroundWorker worker = new BackgroundWorker();
                worker.DoWork += (o, ea) =>
                {
                    foreach (JiraVuIssue issue in issuesToWorkOn)
                    {
                        Dispatcher.Invoke((Action)(() =>
                            issue.OperationState = Enums.OperationStates.InProcess));
                        ProcessIssuesMethod(issue);
                    }
                };
                worker.RunWorkerCompleted += (o, ea) =>
                {
                    progressBar1.Visibility = System.Windows.Visibility.Hidden;
                };
                progressBar1.Visibility = System.Windows.Visibility.Visible;
                worker.RunWorkerAsync();
            }
            catch (Exception) { }
        }

        #endregion
    }
}
