﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using JiraVu.Core;

namespace JiraVu.Views
{
    /// <summary>
    /// Interaction logic for JiraVuTabUserControl.xaml
    /// </summary>
    public partial class JiraVuTabUserControl : UserControl, INavigationUserControl, INotifyPropertyChanged
    {
        private ObservableCollection<TabItem> _tabs;
        private UserControl _ucBackControl;
        private UserControl _ucNextControl;

        #region Public Properties

        public MainWindow ParentWindow { get; set; }

        public ObservableCollection<TabItem> Tabs
        {
            get
            {
                if (_tabs == null)
                    _tabs = new ObservableCollection<TabItem>();
                return _tabs;
            }
            set
            {
                _tabs = value; RaisePropertyChanged("Tabs");
            }
        }

        public UserControl BackUserControl
        {
            get
            {
                if (_ucBackControl == null)
                    _ucBackControl = new MainMenuUserControl(ParentWindow);
                return _ucBackControl;
            }
            set
            {
                _ucBackControl = value;
            }
        }

        public UserControl NextUserControl
        {
            get
            {
                if (_ucNextControl == null)
                    _ucNextControl = new MainMenuUserControl(ParentWindow);
                return _ucNextControl;
            }
            set
            {
                _ucNextControl = value;
            }
        }

        #endregion Public Properties

        #region Public Methods

        public bool DoWorkAsync()
        {
            return true;
        }

        public bool PrepMoveBack()
        {
            // Prepare for hand-off to the previous user control
            ((INavigationUserControl)BackUserControl).NextUserControl = this;
            return ((INavigationUserControl)BackUserControl).UpdateParentUI();
        }

        public bool PrepMoveNext()
        {
            // Prepare for hand-off to the next user control
            ((INavigationUserControl)NextUserControl).BackUserControl = this;
            return ((INavigationUserControl)NextUserControl).UpdateParentUI();
        }

        public bool UpdateParentUI()
        {
            ParentWindow.BackButton.IsEnabled = true;
            ParentWindow.NextButton.IsEnabled = true;
            return true;
        }

        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion INotifyPropertyChanged

        #region Constructors

        public JiraVuTabUserControl()
        {
            InitializeComponent();
        }

        public JiraVuTabUserControl(MainWindow parentWindow)
            : this()
        {
            ParentWindow = parentWindow;

            ObservableCollection<TabItem> initialTabs = new ObservableCollection<TabItem>();
            TabItem initialTab = new TabItem();
            initialTab.Header = "New Tree";
            initialTab.Content = new IssueTreeViewUserControl(this.ParentWindow, initialTab);

            TabItem tabNew = new TabItem();
            tabNew.Header = "+";
            tabNew.PreviewMouseDown += tabNew_PreviewMouseDown;

            initialTabs.Add(initialTab);
            initialTabs.Add(tabNew);

            Tabs = initialTabs;
            tabControl.ItemsSource = Tabs;
        }

        void tabNew_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            TabItem newTabItem = new TabItem();
            newTabItem.Header = "New Tree";
            newTabItem.Content = new IssueTreeViewUserControl(this.ParentWindow, newTabItem);
            Tabs.Insert(Tabs.Count - 1, newTabItem);
        }

        #endregion Constructors

    }
}
