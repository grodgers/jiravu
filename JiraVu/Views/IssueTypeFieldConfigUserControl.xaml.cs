﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using JiraRestClient;
using JiraVu.Core;

namespace JiraVu.Views
{
    /// <summary>
    /// Interaction logic for IssueTypeFieldConfigUserControl.xaml
    /// </summary>
    public partial class IssueTypeFieldConfigUserControl : UserControl, INavigationUserControl
    {
        private int intProjectIndex = -1;
        private int intIssueTypeScreenIndex = -1;
        private UserControl _ucBackControl;
        private UserControl _ucNextControl;
        private UserControl _ucOrigNextControl;

        #region Constructors

        public IssueTypeFieldConfigUserControl(MainWindow parentWindow)
        {
            InitializeComponent();
            this.ParentWindow = parentWindow;
        }

        #endregion

        #region Public Properties

        public IssueTypeFieldSelectionUserControl IssueTypeFieldSelectionUserControl { get; set; }
        public MainWindow ParentWindow { get; set; }
        public UserControl BackUserControl
        {
            get
            {
                return _ucBackControl;
            }
            set
            {
                _ucBackControl = value;
            }
        }
        public UserControl NextUserControl
        {
            get
            {
                return _ucNextControl;
            }
            set
            {
                _ucNextControl = value;
            }
        }

        #endregion

        #region Public Methods

        public bool DoWorkAsync()
        {
            // TODO: Look into if this explicit overwrite is necessary, or if binding is taking care of it
            if (this.IssueTypeFieldSelectionUserControl != null)
            {   // Overwrite issuetype field selection information with updated user selections
                this.ParentWindow.SelectedIssueCreationMeta.Projects[intProjectIndex].IssueTypes[intIssueTypeScreenIndex] =
                    IssueTypeFieldSelectionUserControl.JiraVuIssueType;
            }
            return true;
        }

        public bool PrepMoveBack()
        {
            // Prepare for hand-off to the previous user control
            ((INavigationUserControl)BackUserControl).NextUserControl = this;
            return ((INavigationUserControl)BackUserControl).UpdateParentUI();
        }

        public bool PrepMoveNext()
        {
            if (intProjectIndex < ParentWindow.SelectedIssueCreationMeta.Projects.Count)
            {
                // Start with the assumption that we still need to be on this control with more info to collect
                this.NextUserControl = this;

                // Determine if we have more issue types to work through for the current project
                if (++intIssueTypeScreenIndex < ParentWindow.SelectedIssueCreationMeta.Projects[intProjectIndex].IssueTypes.Count)
                {   // Set sub-user control to next issuetype field selector within this project
                    this.IssueTypeFieldSelectionUserControl = new IssueTypeFieldSelectionUserControl(this,
                        ParentWindow.SelectedIssueCreationMeta.Projects[intProjectIndex],
                        ParentWindow.SelectedIssueCreationMeta.Projects[intProjectIndex].IssueTypes[intIssueTypeScreenIndex]);
                }
                else if (++intProjectIndex < ParentWindow.SelectedIssueCreationMeta.Projects.Count)
                {   // No more issue types for this project, so see if another project is available
                    // Reset issuetype counter
                    this.intIssueTypeScreenIndex = 0;
                    // Set sub-user control to next issuetype field selector for next project
                    this.IssueTypeFieldSelectionUserControl = new IssueTypeFieldSelectionUserControl(this,
                        ParentWindow.SelectedIssueCreationMeta.Projects[intProjectIndex],
                        ParentWindow.SelectedIssueCreationMeta.Projects[intProjectIndex].IssueTypes[intIssueTypeScreenIndex]);
                }
                else
                {   // No more issue types to work through
                    this.IssueTypeFieldSelectionUserControl = null;
                    this.NextUserControl = _ucOrigNextControl; //new IssueWorkProgressUserControl(this.ParentWindow, "Create Issue Sub-tasks", CreateIssueSubtask);
                    ((INavigationUserControl)NextUserControl).BackUserControl = this;
                }
            }

            // Add the issue type field selection control to the body of this control
            if (this.IssueTypeFieldSelectionUserControl != null)
            {
                this.grdUserControl.Children.Clear();
                this.grdUserControl.Children.Add(this.IssueTypeFieldSelectionUserControl);
            }

            return ((INavigationUserControl)NextUserControl).UpdateParentUI();
        }

        public bool UpdateParentUI()
        {
            try
            {
                ParentWindow.BackButton.IsEnabled = true;
                ParentWindow.NextButton.IsEnabled = true;
            }
            catch (Exception) { }
            return true;
        }
        #endregion

        #region Event Handlers

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            // Load the first issue type field selection control
            intProjectIndex = 0;
            _ucOrigNextControl = NextUserControl;
            PrepMoveNext();
        }

        #endregion
    }
}
