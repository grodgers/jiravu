﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using JiraRestClient;
using JiraVu.Core;

namespace JiraVu.Views
{
    /// <summary>
    /// Interaction logic for IssueTypeTraceUserControl.xaml
    /// </summary>
    public partial class IssueTypeTraceUserControl : UserControl, INavigationUserControl
    {
        private UserControl _ucBackControl;
        private UserControl _ucNextControl;

        #region Public Properties

        public List<JiraVuIssueType> AvailableIssueTypes { get; set; }
        public ObservableCollection<JiraVuIssueType> SelectedIssueTypes { get; set; }
        public MainWindow ParentWindow { get; set; }
        public UserControl BackUserControl
        {
            get
            {
                return _ucBackControl;
            }
            set
            {
                _ucBackControl = value;
            }
        }
        public UserControl NextUserControl
        {
            get
            {
                if (_ucNextControl == null) _ucNextControl = new IssueStatusColorMapUserControl(this.ParentWindow);
                return _ucNextControl;
            }
            set
            {
                _ucNextControl = value;
            }
        }

        #endregion

        #region Constructors

        public IssueTypeTraceUserControl(MainWindow parentWindow)
        {
            InitializeComponent();
            this.DataContext = this;
            this.ParentWindow = parentWindow;
            this.SelectedIssueTypes = new ObservableCollection<JiraVuIssueType>();
            // Populate combo box with available issue types
            if (this.AvailableIssueTypes == null)
            {
                this.AvailableIssueTypes = new List<JiraVuIssueType>();
                foreach (JiraIssueType jiraIssueType in ParentWindow.RestClient.GetIssueTypes())
                {
                    this.AvailableIssueTypes.Add(new JiraVuIssueType(jiraIssueType));
                }
            }
            dgIssueTypesAvailable.ItemsSource = this.AvailableIssueTypes;
            dgIssueTypesSelected.ItemsSource = this.SelectedIssueTypes;
        }

        #endregion

        #region Public Methods

        public bool DoWorkAsync()
        {
            ParentWindow.SelectedIssueTypes = new List<JiraVuIssueType>();
            foreach (JiraVuIssueType issueType in SelectedIssueTypes)
            {
                ParentWindow.SelectedIssueTypes.Add(issueType);
            }
            return true;
        }

        public bool PrepMoveBack()
        {
            // Prepare for hand-off to the previous user control
            ((INavigationUserControl)BackUserControl).NextUserControl = this;
            return ((INavigationUserControl)BackUserControl).UpdateParentUI();
        }

        public bool PrepMoveNext()
        {
            // Prepare for hand-off to the next user control
            ((INavigationUserControl)NextUserControl).BackUserControl = this;
            return ((INavigationUserControl)NextUserControl).UpdateParentUI();
        }

        public bool UpdateParentUI()
        {
            ParentWindow.BackButton.IsEnabled = true;
            ParentWindow.NextButton.IsEnabled = true;
            return true;
        }

        #endregion

        #region Private Methods

        private void AddSelectedIssueTypes()
        {
            for (int i = 0; i < dgIssueTypesAvailable.SelectedCells.Count; i++)
            {
                JiraVuIssueType jvitToAdd = new JiraVuIssueType(
                    ((JiraVuIssueType)dgIssueTypesAvailable.SelectedCells[i].Item).JiraIssueType);
                jvitToAdd.TraceIndex = SelectedIssueTypes.Count;
                SelectedIssueTypes.Add(jvitToAdd);
            }
        }

        private void RemoveAllIssueTypes()
        {
            SelectedIssueTypes.Clear();
        }

        private void RemoveSelectedIssueTypes()
        {
            for (int i = dgIssueTypesSelected.SelectedItems.Count - 1; i >= 0; i--)
            {
                SelectedIssueTypes.RemoveAt(((JiraVuIssueType)dgIssueTypesSelected.SelectedItems[i]).TraceIndex);
                RenumberTraceIndexes();
            }
        }

        private void RenumberTraceIndexes()
        {
            for (int i = 0; i < SelectedIssueTypes.Count; i++)
            {
                SelectedIssueTypes[i].TraceIndex = i;
            }
        }

        #endregion

        #region Event Handlers

        private void btnAddToSelected_Click(object sender, RoutedEventArgs e)
        {
            AddSelectedIssueTypes();
        }

        private void btnRemoveAll_Click(object sender, RoutedEventArgs e)
        {
            RemoveAllIssueTypes();
        }

        private void btnRemoveSelected_Click(object sender, RoutedEventArgs e)
        {
            RemoveSelectedIssueTypes();
        }

        private void dgIssueTypesAvailable_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            AddSelectedIssueTypes();
        }

        private void dgIssueTypesSelected_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            RemoveSelectedIssueTypes();
        }

        #endregion

    }
}
