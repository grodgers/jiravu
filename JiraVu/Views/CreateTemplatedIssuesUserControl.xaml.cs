﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using JiraRestClient;
using JiraVu.Converters;
using JiraVu.Core;
using Excel;

namespace JiraVu.Views
{
    /// <summary>
    /// Interaction logic for CreateTemplatedIssuesUserControl.xaml
    /// </summary>
    public partial class CreateTemplatedIssuesUserControl : UserControl, INavigationUserControl, INotifyPropertyChanged
    {
        private DataTable _dtVariableData;
        private string _selectedFilePath;
        private UserControl _ucBackControl;
        private UserControl _ucNextControl;

        #region Public Properties

        public DataTable VariableData
        {
            get
            {
                if (_dtVariableData == null)
                {
                    _dtVariableData = new DataTable();
                    _dtVariableData.Columns.Add("{Y} Values");
                }
                return _dtVariableData;
            }
            set
            {
                _dtVariableData = value;
                NotifyPropertyChanged("VariableData");
            }
        }

        public string SelectedFilePath
        {
            get { return _selectedFilePath; }
            set
            {
                _selectedFilePath = value;
                NotifyPropertyChanged("SelectedFilePath");
            }
        }

        public MainWindow ParentWindow { get; set; }

        public UserControl BackUserControl
        {
            get
            {
                return _ucBackControl;
            }
            set
            {
                _ucBackControl = value;
            }
        }

        public UserControl NextUserControl
        {
            get
            {
                return _ucNextControl;
            }
            set
            {
                _ucNextControl = value;
            }
        }

        #endregion

        #region Constructors

        public CreateTemplatedIssuesUserControl(MainWindow parentWindow)
        {
            InitializeComponent();
            DataContext = this;
            this.ParentWindow = parentWindow;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Notify clients, typically binding clients, that a property value has changed.
        /// </summary>
        /// <param name="info">The property that has changed</param>
        private void NotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(info));
        }

        private void TransformVariableDataTable()
        {
            if (VariableData.Columns.Count < 1) return;

            // Clear auto-generated columns
            dgVariableData.Columns.Clear();

            bool useStarWidth = VariableData.Columns.Count <= 5;

            // Build the common column header style
            Style headerStyle = new Style();
            headerStyle.Setters.Add(new Setter(HorizontalContentAlignmentProperty, HorizontalAlignment.Center));

            // Build the element style for the column
            Style elemStyle = new Style(typeof(TextBlock));
            elemStyle.Setters.Add(new Setter(HorizontalAlignmentProperty, HorizontalAlignment.Center));
            elemStyle.Setters.Add(new Setter(VerticalAlignmentProperty, VerticalAlignment.Center));

            DataGridTextColumn yClmn = new DataGridTextColumn();
            yClmn.Header = "";
            yClmn.Binding = new Binding(VariableData.Columns[0].ColumnName);
            if (useStarWidth) 
                yClmn.Width = new DataGridLength(50, DataGridLengthUnitType.Star);
            else
                yClmn.Width = new DataGridLength(50, DataGridLengthUnitType.Auto);
            yClmn.CanUserSort = false;
            yClmn.HeaderStyle = headerStyle;
            yClmn.ElementStyle = elemStyle;
            dgVariableData.Columns.Add(yClmn);
            
            if (VariableData.Columns.Count > 1)
            {
                // Convert all cell values to binary, depending on if text is found
                foreach (DataRow row in VariableData.Rows)
                {
                    for (int i = 1; i < VariableData.Columns.Count; i++)
                    {
                        row[i] = (row[i].ToString() != String.Empty) ? true : false;
                    }
                }

                // Build checkbox columns to represent desired variable intersections
                for (int i = 1; i < VariableData.Columns.Count; i++)
                {
                    DataGridCheckBoxColumn xClmn = new DataGridCheckBoxColumn();
                    xClmn.Header = VariableData.Columns[i].ColumnName;
                    xClmn.Binding = new Binding(VariableData.Columns[i].ColumnName);
                    if (useStarWidth) 
                        xClmn.Width = new DataGridLength(50, DataGridLengthUnitType.Star);

                    // Rebuild element style
                    elemStyle = new Style(typeof(CheckBox));
                    elemStyle.Setters.Add(new Setter(HorizontalAlignmentProperty, HorizontalAlignment.Center));
                    elemStyle.Setters.Add(new Setter(VerticalAlignmentProperty, VerticalAlignment.Center));

                    xClmn.ElementStyle = elemStyle;
                    xClmn.HeaderStyle = headerStyle;
                    dgVariableData.Columns.Add(xClmn);
                }
            }

            dgVariableData.CanUserAddRows = false;
        }

        #endregion

        #region Public Methods

        public void CreateIssueFromTemplate(JiraVuIssue templateIssue, string xValue, string yValue)
        {
            try
            {
                JiraIssueGeneric newGenericIssue = new JiraIssueGeneric();
                newGenericIssue.fields.Add("project", templateIssue.JiraIssue.fields.project);
                newGenericIssue.fields.Add("issuetype", templateIssue.JiraIssue.fields.issuetype);
                newGenericIssue.fields.Add("summary", FieldValueConverter.ReplaceParams(
                    templateIssue.JiraIssue.fields.summary, xValue, yValue));
                if (templateIssue.JiraIssue.fields.parent != null)
                    newGenericIssue.fields.Add("parent", templateIssue.JiraIssue.fields.parent);
                // Create the issue using the basic information
                JiraIssue newIssue = ParentWindow.RestClient.CreateIssue(newGenericIssue);
                // Clear the generic issue's fields for update
                newGenericIssue.fields = new Dictionary<string, object>();
                // Get the issue creation metadata for this issue type and project
                JiraVuIssueType issueTypeFieldMeta = this.ParentWindow.SelectedIssueCreationMeta.GetIssueTypeForProject(
                    templateIssue.JiraIssue.fields.project.key,
                    templateIssue.JiraIssue.fields.issuetype.name);
                // Get properties from template issue's fields
                string propertyName = "";
                object propertyValue = null;
                // Copy fields selected by user
                foreach (JiraVuIssueTypeFieldMeta fieldMeta in issueTypeFieldMeta.Fields)
                {
                    if (fieldMeta.IsSelected)
                    {   // Copy data from template to new issue for selected field
                        if (fieldMeta.JiraIssueTypeFieldMeta.schema.system != null)
                        {   // system field
                            propertyName = fieldMeta.JiraIssueTypeFieldMeta.schema.system;
                        }
                        else if (fieldMeta.JiraIssueTypeFieldMeta.schema.custom != null)
                        {   // custom field
                            propertyName = "customfield_" + fieldMeta.JiraIssueTypeFieldMeta.schema.customId;
                        }
                        propertyValue = FieldValueConverter.Convert(fieldMeta.JiraIssueTypeFieldMeta.schema,
                            templateIssue.JiraIssue.AllFields[propertyName], xValue, yValue);
                        newGenericIssue.fields.Add(propertyName, propertyValue);
                    }
                    else if (fieldMeta.IsValueSupplied)
                    {   // Copy data from what was supplied by user
                        if (fieldMeta.JiraIssueTypeFieldMeta.schema.system != null)
                        {   // system field
                            propertyName = fieldMeta.JiraIssueTypeFieldMeta.schema.system;
                        }
                        else if (fieldMeta.JiraIssueTypeFieldMeta.schema.custom != null)
                        {   // custom field
                            propertyName = "customfield_" + fieldMeta.JiraIssueTypeFieldMeta.schema.customId;
                        }
                        //TODO: Fix the next line to send values from loaded spreadsheet
                        propertyValue = FieldValueConverter.Convert(fieldMeta.JiraIssueTypeFieldMeta.schema,
                            fieldMeta.FieldValue, xValue, yValue);
                        newGenericIssue.fields.Add(propertyName, propertyValue);
                    }
                }
                // Update the issue with the other fields values copied from parent issue
                if (ParentWindow.RestClient.UpdateIssue(newIssue.key, newGenericIssue))
                {
                    templateIssue.OperationState = Enums.OperationStates.Success;
                }
                else
                {
                    templateIssue.OperationState = Enums.OperationStates.Failure;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                templateIssue.OperationState = Enums.OperationStates.Failure;
            }
        }

        public void CreateIssuesFromTemplate(JiraVuIssue templateIssue)
        {
            try
            {
                if (_dtVariableData == null) return; //TODO: throw exception
                if (_dtVariableData.Columns.Count == 0) return; //TODO: throw exception

                if (_dtVariableData.Columns.Count == 1)
                {   // Using a list template, assuming first row is for headers
                    foreach (DataRow row in VariableData.Rows)
                    {
                        CreateIssueFromTemplate(templateIssue, null, row[0].ToString());
                    }
                }
                else if (_dtVariableData.Columns.Count > 1)
                {   // Using a matrix template, assuming first row is for headers
                    foreach (DataRow row in VariableData.Rows)
                    {
                        for (int col = 1; col < _dtVariableData.Columns.Count; col++)
                        {
                            try
                            {
                                if (Convert.ToBoolean(row[col]))
                                {
                                    CreateIssueFromTemplate(templateIssue, _dtVariableData.Columns[col].ColumnName, row[0].ToString()); 
                                }
                            }
                            catch (Exception) { }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                templateIssue.OperationState = Enums.OperationStates.Failure;
            }
        }

        public bool DoWorkAsync()
        {
            return true;
        }

        public bool PrepMoveBack()
        {
            // Prepare for hand-off to the previous user control
            ((INavigationUserControl)BackUserControl).NextUserControl = this;
            return ((INavigationUserControl)BackUserControl).UpdateParentUI();
        }

        public bool PrepMoveNext()
        {
            // Prepare for hand-off to the next user control
            ((INavigationUserControl)NextUserControl).BackUserControl = this;
            return ((INavigationUserControl)NextUserControl).UpdateParentUI();
        }

        public bool UpdateParentUI()
        {
            ParentWindow.BackButton.IsEnabled = true;
            ParentWindow.NextButton.IsEnabled = true;
            return true;
        }

        #endregion

        #region Event Handlers

        public event PropertyChangedEventHandler PropertyChanged;

        private void btnBrowse_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlgOpenFile = new Microsoft.Win32.OpenFileDialog();
            dlgOpenFile.Filter = "Excel Files (*.xls, *.xlsx)|*.xls;*.xlsx";
            dlgOpenFile.Multiselect = false;
            dlgOpenFile.Title = "Select an Excel File to Open";

            var result = dlgOpenFile.ShowDialog();
            IExcelDataReader excelReader = null;

            try
            {
                if (Convert.ToBoolean(result) == true)
                {
                    string fileName = dlgOpenFile.FileName;
                    SelectedFilePath = fileName;
                    string fileExt = fileName.Substring(fileName.LastIndexOf('.') + 1).ToLower();

                    FileStream stream = File.Open(dlgOpenFile.FileName, FileMode.Open, FileAccess.Read);

                    switch (fileExt)
                    {
                        case "xls":
                            // Reading from a binary Excel file ('97-2003 format; *.xls)
                            excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
                            break;
                        case "xlsx":
                            // Reading from a OpenXml Excel file (2007 format; *.xlsx)
                            excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                            break;
                        default:
                            break;
                    }

                    if (excelReader != null)
                    {
                        excelReader.IsFirstRowAsColumnNames = true;
                        DataSet ds = excelReader.AsDataSet();
                        VariableData = ds.Tables[0];
                        TransformVariableDataTable();
                    }
                }
            }
            catch (Exception) 
            {
                //TODO: Some notification to user and possibly log the issue 
            }
            finally
            {
                if (excelReader != null)
                {
                    excelReader.Close();
                    excelReader = null;
                }
            }
        }

        #endregion
        
    }
}
