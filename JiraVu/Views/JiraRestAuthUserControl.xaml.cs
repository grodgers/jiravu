﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using JiraVu.Core;
using JiraRestClient;

namespace JiraVu.Views
{
    /// <summary>
    /// Interaction logic for JiraRestAuthUserControl.xaml
    /// </summary>
    public partial class JiraRestAuthUserControl : UserControl, INavigationUserControl
    {
        private UserControl _ucBackControl;
        private UserControl _ucNextControl;

        #region Public Properties

        public MainWindow ParentWindow { get; set; }

        public UserControl BackUserControl
        {
            get 
            {
                return _ucBackControl; 
            }
            set 
            { 
                _ucBackControl = value; 
            }
        }

        public UserControl NextUserControl
        {
            get 
            {
                if (_ucNextControl == null) _ucNextControl = new JiraVuTabUserControl(ParentWindow);
                return _ucNextControl; 
            }
            set
            {
                _ucNextControl = value;
            }
        }

        #endregion

        #region Constructors

        public JiraRestAuthUserControl(MainWindow parentWindow)
        {
            InitializeComponent();
            this.ParentWindow = parentWindow;
        }

        #endregion

        #region Public Methods

        public bool DoWorkAsync()
        {
            ParentWindow.RestClient = new JiraRestClient.JiraRestClient(
                txtRestBaseUrl.Text.Trim(),
                txtUsername.Text.Trim(),
                txtPassword.Password);

            List<JiraUser> users = null;

            try
            {
                users = ParentWindow.RestClient.GetUsersBySearch(txtUsername.Text.Trim());
            }
            catch (ArgumentException ae)
            {
                ParentWindow.ShowUserNotification(ae.Message);
                return false;
            }
            catch (UnauthorizedAccessException)
            {
                ParentWindow.ShowUserNotification("Invalid credentials." + Environment.NewLine +
                                                  "Please check the username and password, and then try again.");
                return false;
            }
            catch (Exception)
            {
                ParentWindow.ShowUserNotification("Please check the URL, username and password, and then try again.");
                return false;
            }

            return users.Count > 0;
        }

        public bool PrepMoveBack()
        {
            return false;
        }

        public bool PrepMoveNext()
        {
            // Prepare for hand-off to the next user control
            ParentWindow.HideUserNotification();
            ((INavigationUserControl)NextUserControl).BackUserControl = this;
            return ((INavigationUserControl)NextUserControl).UpdateParentUI();
        }

        public bool UpdateParentUI()
        {
            ParentWindow.BackButton.IsEnabled = false;
            ParentWindow.NextButton.IsEnabled = true;
            return true;
        }

        #endregion

        #region Event Handlers

        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                ParentWindow.MoveNext();
        }

        private void txtPassword_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (ParentWindow != null && txtPassword.IsFocused)
                ParentWindow.HideUserNotification();
        }

        private void txtRestBaseUrl_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (ParentWindow != null && txtRestBaseUrl.IsFocused)
                ParentWindow.HideUserNotification();
        }

        private void txtUsername_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (ParentWindow != null && txtUsername.IsFocused)
                ParentWindow.HideUserNotification();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (txtRestBaseUrl.Text.Trim() == "https://yourcompany.atlassian.net/rest/api/latest")
            {
                txtRestBaseUrl.Select(8, 11);
                txtRestBaseUrl.Focus();
            }
            else if (txtRestBaseUrl.Text.Trim() != String.Empty)
            {
                if (txtUsername.Text.Trim() != String.Empty)
                {
                    txtPassword.Focus();
                }
                else
                {
                    txtUsername.Focus();
                }
            }
        }

        #endregion

    }
}
