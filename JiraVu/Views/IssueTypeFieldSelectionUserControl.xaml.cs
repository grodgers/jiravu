﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using JiraVu.Core;

namespace JiraVu.Views
{
    /// <summary>
    /// Interaction logic for IssueTypeFieldSelectionUserControl.xaml
    /// </summary>
    public partial class IssueTypeFieldSelectionUserControl : UserControl, INavigationUserControl
    {
        private UserControl _ucBackControl;
        private UserControl _ucNextControl;

        #region Public Properties

        public JiraVuIssueType JiraVuIssueType { get; set; }
        public JiraVuProject JiraVuProject { get; set; }
        public MainWindow ParentWindow { get; set; }
        public UserControl BackUserControl
        {
            get
            {
                return _ucBackControl;
            }
            set
            {
                _ucBackControl = value;
            }
        }
        public UserControl NextUserControl
        {
            get
            {   
                return _ucNextControl;
            }
            set
            {
                _ucNextControl = value;
            }
        }
        public UserControl ParentControl { get; set; }

        #endregion

        #region Constructors

        public IssueTypeFieldSelectionUserControl(UserControl parentControl, JiraVuProject jiraVuProject, JiraVuIssueType jiraVuIssueType)
        {
            InitializeComponent();
            this.ParentControl = parentControl;
            this.JiraVuProject = jiraVuProject;
            this.JiraVuIssueType = jiraVuIssueType;
            dgIssueTypeFields.ItemsSource = jiraVuIssueType.Fields;
            txtProjectKey.Text = jiraVuProject.JiraProject.key;
            txtIssueType.Text = jiraVuIssueType.JiraIssueType.name;
        }

        #endregion

        #region Public Methods

        public bool DoWorkAsync()
        {
            return true;
        }

        public bool PrepMoveNext()
        {
            return true;
        }

        public bool PrepMoveBack()
        {
            return true;
        }

        public bool UpdateParentUI()
        {
            return true;
        }

        #endregion

        #region Event Handlers

        private void chkHeader_Checked(object sender, RoutedEventArgs e)
        {
            foreach (JiraVuIssueTypeFieldMeta fieldMeta in JiraVuIssueType.JiraIssueType.fields.Values)
            {
                if (!fieldMeta.JiraIssueTypeFieldMeta.required)
                {
                    fieldMeta.IsSelected = true;
                }
            }
        }

        private void chkHeader_Unchecked(object sender, RoutedEventArgs e)
        {
            foreach (JiraVuIssueTypeFieldMeta fieldMeta in JiraVuIssueType.JiraIssueType.fields.Values)
            {
                if (!fieldMeta.JiraIssueTypeFieldMeta.required)
                {
                    fieldMeta.IsSelected = false;
                }
            }
        }

        private void chkItem_Checked(object sender, RoutedEventArgs e)
        {
            if (((CheckBox)sender).IsFocused)
            {
                ((JiraVuIssueTypeFieldMeta)dgIssueTypeFields.SelectedItem).IsSelected = true;
                ((JiraVuIssueTypeFieldMeta)dgIssueTypeFields.SelectedItem).IsValueSupplied = false;
            }
        }

        private void chkItem_Unchecked(object sender, RoutedEventArgs e)
        {
            if (((CheckBox)sender).IsFocused)
            {
                ((JiraVuIssueTypeFieldMeta)dgIssueTypeFields.SelectedItem).IsSelected = false;
            }
        }

        private void chkValue_Checked(object sender, RoutedEventArgs e)
        {
            if (((CheckBox)sender).IsFocused)
            {
                ((JiraVuIssueTypeFieldMeta)dgIssueTypeFields.SelectedItem).IsSelected = false;
                ((JiraVuIssueTypeFieldMeta)dgIssueTypeFields.SelectedItem).IsValueSupplied = true;
            }
        }

        private void chkValue_Unchecked(object sender, RoutedEventArgs e)
        {
            if (((CheckBox)sender).IsFocused)
            {
                ((JiraVuIssueTypeFieldMeta)dgIssueTypeFields.SelectedItem).IsValueSupplied = false;
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            foreach (JiraVuIssueTypeFieldMeta fieldMeta in JiraVuIssueType.Fields)
            {
                if (fieldMeta.JiraIssueTypeFieldMeta.required)
                {
                    fieldMeta.IsSelected = true;
                }
            }
        }

        #endregion
    }
}
