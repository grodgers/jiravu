﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using JiraRestClient;
using JiraVu.Views;
using JiraVu.Core;

namespace JiraVu.Views
{
    /// <summary>
    /// Interaction logic for IssueOperationsUserControl.xaml
    /// </summary>
    public partial class IssueOperationsUserControl : UserControl, INavigationUserControl
    {
        private UserControl _ucBackControl;
        private UserControl _ucNextControl;

        #region Constructors

        public IssueOperationsUserControl(MainWindow parentWindow)
        {
            InitializeComponent();
            this.DataContext = this;
            this.ParentWindow = parentWindow;
        }

        #endregion

        #region Public Properties

        public MainWindow ParentWindow { get; set; }
        public UserControl BackUserControl
        {
            get
            {
                return _ucBackControl;
            }
            set
            {
                _ucBackControl = value;
            }
        }
        public UserControl NextUserControl
        {
            get
            {
                if (_ucNextControl == null)
                {
                    switch (((JiraVuListBoxItem)lbOperations.SelectedItem).ItemValue)
                    {
                        case "CreateSubtasksUserControl":
                            // Plot the path for this operation
                            IssueTypeSelectionUserControl uc1_1 = new IssueTypeSelectionUserControl(this.ParentWindow, JiraVuIssueType.IssueTypeClasses.Subtask);
                            _ucNextControl = uc1_1;
                            uc1_1.BackUserControl = this;
                            CreateSubtasksUserControl uc1_2 = new CreateSubtasksUserControl(this.ParentWindow); //TODO: Replace with IssueTypeFieldConfigUserControl
                            uc1_1.NextUserControl = uc1_2;
                            uc1_2.BackUserControl = uc1_1;
                            //TODO: Add IssueWorkProgressUserControl to explicit operation path
                            uc1_2.NextUserControl = new IssueNavigatorUserControl(this.ParentWindow);
                            break;
                        case "IssueLinkTraceabilityMatrix":
                            // Plot the path for this operation
                            IssueTypeTraceUserControl uc2_1 = new IssueTypeTraceUserControl(this.ParentWindow);
                            _ucNextControl = uc2_1;
                            uc2_1.BackUserControl = this;
                            IssueStatusColorMapUserControl uc2_2 = new IssueStatusColorMapUserControl(this.ParentWindow);
                            uc2_1.NextUserControl = uc2_2;
                            uc2_2.BackUserControl = uc2_1;
                            break;
                        case "CreateTemplatedIssues":
                            // Plot the path for this operation
                            CreateTemplatedIssuesUserControl uc3_1 = new CreateTemplatedIssuesUserControl(this.ParentWindow);
                            _ucNextControl = uc3_1;
                            uc3_1.BackUserControl = this;
                            IssueTypeFieldConfigUserControl uc3_2 = new IssueTypeFieldConfigUserControl(this.ParentWindow);
                            uc3_1.NextUserControl = uc3_2;
                            uc3_2.BackUserControl = uc3_1;
                            uc3_2.NextUserControl = new IssueWorkProgressUserControl(this.ParentWindow, "Create Templated Issues", uc3_1.CreateIssuesFromTemplate);
                            break;
                        case "IssueTreeView":
                            // Plot the path for this operation
                            IssueTreeViewUserControl uc4_1 = new IssueTreeViewUserControl(this.ParentWindow);
                            _ucNextControl = uc4_1;
                            uc4_1.BackUserControl = this;
                            break;
                    }
                }
                return _ucNextControl;
            }
            set
            {
                _ucNextControl = value;
            }
        }

        #endregion

        #region Public Methods

        public bool DoWorkAsync()
        {
            return true;
        }

        public bool PrepMoveBack()
        {
            // Prepare for hand-off to the previous user control
            ((INavigationUserControl)BackUserControl).NextUserControl = this;
            return ((INavigationUserControl)BackUserControl).UpdateParentUI();
        }

        public bool PrepMoveNext()
        {
            // Prepare for hand-off to the next user control
            ((INavigationUserControl)NextUserControl).BackUserControl = this;
            return ((INavigationUserControl)NextUserControl).UpdateParentUI();
        }

        public bool UpdateParentUI()
        {
            ParentWindow.BackButton.IsEnabled = true;
            ParentWindow.NextButton.IsEnabled = true;
            return true;
        }

        #endregion

        #region Private Methods

        #endregion

        #region Event Handlers

        private void lbOperations_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ParentWindow.MoveNext();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Thickness margin = new Thickness(10, 5, 10, 5);

            lbOperations.Items.Clear();
            JiraVuListBoxItem newItem = new JiraVuListBoxItem("Create Sub-tasks for Selected Issues", "CreateSubtasksUserControl");
            newItem.Margin = margin;
            lbOperations.Items.Add(newItem);

            newItem = new JiraVuListBoxItem("Create Templated Issues from Selected Issues", "CreateTemplatedIssues");
            newItem.Margin = margin;
            lbOperations.Items.Add(newItem);

            newItem = new JiraVuListBoxItem("Issue Link Traceability Matrix", "IssueLinkTraceabilityMatrix");
            newItem.Margin = margin;
            lbOperations.Items.Add(newItem);

            lbOperations.SelectedIndex = 0;
        }

        #endregion
    }
}
