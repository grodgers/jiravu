﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using JiraRestClient;
using JiraVu.Converters;
using JiraVu.Core;

namespace JiraVu.Views
{
    /// <summary>
    /// Interaction logic for CreateSubtasksUserControl.xaml
    /// </summary>
    public partial class CreateSubtasksUserControl : UserControl, INavigationUserControl
    {
        private int intProjectIndex = -1;
        private int intIssueTypeScreenIndex = -1;
        private UserControl _ucBackControl;
        private UserControl _ucNextControl;

        #region Constructors

        public CreateSubtasksUserControl(MainWindow parentWindow)
        {
            InitializeComponent();
            this.ParentWindow = parentWindow;
        }

        #endregion

        #region Public Properties

        public IssueTypeFieldSelectionUserControl IssueTypeFieldSelectionUserControl { get; set; }
        public MainWindow ParentWindow { get; set; }
        public UserControl BackUserControl
        {
            get
            {
                return _ucBackControl;
            }
            set
            {
                _ucBackControl = value;
            }
        }
        public UserControl NextUserControl
        {
            get
            {
                return _ucNextControl;
            }
            set
            {
                _ucNextControl = value;
            }
        }

        #endregion

        #region Public Methods

        public bool DoWorkAsync()
        {
            // TODO: Look into if this explicit overwrite is necessary, or if binding is taking care of it
            if (this.IssueTypeFieldSelectionUserControl != null)
            {   // Overwrite issuetype field selection information with updated user selections
                this.ParentWindow.SelectedIssueCreationMeta.Projects[intProjectIndex].IssueTypes[intIssueTypeScreenIndex] =
                    IssueTypeFieldSelectionUserControl.JiraVuIssueType;
            }
            return true;
        }

        public bool PrepMoveBack()
        {
            // Prepare for hand-off to the previous user control
            ((INavigationUserControl)BackUserControl).NextUserControl = this;
            return ((INavigationUserControl)BackUserControl).UpdateParentUI();
        }

        public bool PrepMoveNext()
        {
            if (intProjectIndex < ParentWindow.SelectedIssueCreationMeta.Projects.Count)
            {
                // Start with the assumption that we still need to be on this control with more info to collect
                this.NextUserControl = this;

                // Determine if we have more issue types to work through for the current project
                if (++intIssueTypeScreenIndex < ParentWindow.SelectedIssueCreationMeta.Projects[intProjectIndex].IssueTypes.Count)
                {   // Set sub-user control to next issuetype field selector within this project
                    this.IssueTypeFieldSelectionUserControl = new IssueTypeFieldSelectionUserControl(this,
                        ParentWindow.SelectedIssueCreationMeta.Projects[intProjectIndex],
                        ParentWindow.SelectedIssueCreationMeta.Projects[intProjectIndex].IssueTypes[intIssueTypeScreenIndex]);
                }
                else if (++intProjectIndex < ParentWindow.SelectedIssueCreationMeta.Projects.Count)
                {   // No more issue types for this project, so see if another project is available
                    // Reset issuetype counter
                    this.intIssueTypeScreenIndex = 0;
                    // Set sub-user control to next issuetype field selector for next project
                    this.IssueTypeFieldSelectionUserControl = new IssueTypeFieldSelectionUserControl(this,
                        ParentWindow.SelectedIssueCreationMeta.Projects[intProjectIndex],
                        ParentWindow.SelectedIssueCreationMeta.Projects[intProjectIndex].IssueTypes[intIssueTypeScreenIndex]);
                }
                else
                {   // No more issue types to work through
                    this.IssueTypeFieldSelectionUserControl = null;
                    this.NextUserControl = new IssueWorkProgressUserControl(this.ParentWindow, "Create Issue Sub-tasks", CreateIssueSubtask);
                    ((INavigationUserControl)NextUserControl).BackUserControl = this;
                }
            }

            // Add the issue type field selection control to the body of this control
            if (this.IssueTypeFieldSelectionUserControl != null)
            {
                this.grdUserControls.Children.Clear();
                this.grdUserControls.Children.Add(this.IssueTypeFieldSelectionUserControl);
            }

            return ((INavigationUserControl)NextUserControl).UpdateParentUI();
        }

        public bool UpdateParentUI()
        {
            try
            {
                ParentWindow.BackButton.IsEnabled = true;
                ParentWindow.NextButton.IsEnabled = true;
            }
            catch (Exception) { }
            return true;
        }

        #endregion

        #region Private Methods

        private void CreateIssueSubtask(JiraVuIssue parentIssue)
        {
            try
            {
                JiraIssueGeneric subtaskIssue = new JiraIssueGeneric();
                subtaskIssue.fields.Add("project", parentIssue.JiraIssue.fields.project);
                subtaskIssue.fields.Add("issuetype", ParentWindow.SelectedIssueType.JiraIssueType);
                subtaskIssue.fields.Add("parent", parentIssue.JiraIssue);
                subtaskIssue.fields.Add("summary", parentIssue.JiraIssue.fields.summary);
                // Create the issue using the basic information
                JiraIssue newSubtaskIssue = ParentWindow.RestClient.CreateIssue(subtaskIssue);
                // Clear the generic issue's fields for update
                subtaskIssue.fields = new Dictionary<string, object>();
                // Get the issue creation metadata for this issue type and project
                JiraVuIssueType issueTypeFieldMeta = this.ParentWindow.SelectedIssueCreationMeta.GetIssueTypeForProject(
                    parentIssue.JiraIssue.fields.project.key,
                    parentIssue.JiraIssue.fields.issuetype.name);
                // Get properties from parent's fields
                string propertyName = "";
                object propertyValue = null;
                // Copy fields selected by user to new subtask
                foreach (JiraVuIssueTypeFieldMeta fieldMeta in issueTypeFieldMeta.Fields)
                {
                    if (fieldMeta.IsSelected)
                    {   // Copy data from parent to child for selected field
                        if (fieldMeta.JiraIssueTypeFieldMeta.schema.system != null)
                        {   // system field
                            propertyName = fieldMeta.JiraIssueTypeFieldMeta.schema.system;
                        }
                        else if (fieldMeta.JiraIssueTypeFieldMeta.schema.custom != null)
                        {   // custom field
                            propertyName = "customfield_" + fieldMeta.JiraIssueTypeFieldMeta.schema.customId;
                        }
                        propertyValue = (parentIssue.JiraIssue.AllFields[propertyName] != null)
                            ? parentIssue.JiraIssue.AllFields[propertyName]
                            : "";
                        subtaskIssue.fields.Add(propertyName, propertyValue);
                    }
                    else if (fieldMeta.IsValueSupplied)
                    {   // Copy data from what was supplied by user
                        if (fieldMeta.JiraIssueTypeFieldMeta.schema.system != null)
                        {   // system field
                            propertyName = fieldMeta.JiraIssueTypeFieldMeta.schema.system;
                        }
                        else if (fieldMeta.JiraIssueTypeFieldMeta.schema.custom != null)
                        {   // custom field
                            propertyName = "customfield_" + fieldMeta.JiraIssueTypeFieldMeta.schema.customId;
                        }
                        propertyValue = FieldValueConverter.Convert(fieldMeta.JiraIssueTypeFieldMeta.schema, fieldMeta.FieldValue);
                        subtaskIssue.fields.Add(propertyName, propertyValue);
                    }
                }
                // Update the issue with the other fields values copied from parent issue
                if (ParentWindow.RestClient.UpdateIssue(newSubtaskIssue.key, subtaskIssue))
                {
                    parentIssue.OperationState = Enums.OperationStates.Success;
                }
                else
                {
                    parentIssue.OperationState = Enums.OperationStates.Failure;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                parentIssue.OperationState = Enums.OperationStates.Failure;
            }
        }

        #endregion

        #region Event Handlers

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            // Load the first issue type field selection control
            this.intProjectIndex = 0;
            PrepMoveNext();
        }

        #endregion

    }
}
