﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using JiraRestClient;
using JiraVu.Converters;
using JiraVu.Core;

namespace JiraVu.Views
{
    /// <summary>
    /// Interaction logic for CreateIssueLinkTraceabilityMatrixUserControl.xaml
    /// </summary>
    public partial class IssueLinkTraceabilityMatrixUserControl : UserControl, INavigationUserControl
    {
        private bool _inclIndirectLinks = true;
        private bool _initialRun = true;
        private DataTable dtMatrix;
        private UserControl _ucBackControl;
        private UserControl _ucNextControl;

        #region Public Properties

        public bool IncludeIndirectLinks
        {
            get { return _inclIndirectLinks; }
            set
            {
                _inclIndirectLinks = value;
            }
        }
        public MainWindow ParentWindow { get; set; }
        public UserControl BackUserControl
        {
            get
            {
                return _ucBackControl;
            }
            set
            {
                _ucBackControl = value;
            }
        }
        public UserControl NextUserControl
        {
            get
            {
                if (_ucNextControl == null) _ucNextControl = new IssueNavigatorUserControl(this.ParentWindow);
                return _ucNextControl;
            }
            set
            {
                _ucNextControl = value;
            }
        }

        #endregion

        #region Constructors

        public IssueLinkTraceabilityMatrixUserControl(MainWindow parentWindow)
        {
            InitializeComponent();
            this.ParentWindow = parentWindow;
            this.DataContext = this;
        }

        #endregion

        #region Private Methods

        private void AddLinkTraceabilityRow(JiraVuIssue issue, ref List<JiraVuIssueType> issueTypes, int issueTypeIndex, List<JiraVuIssue> linkedIssues)
        {
            if (issueTypes == null) return;
            issueTypeIndex++;
            if (issueTypeIndex >= issueTypes.Count || issue.JiraIssue.fields.issuelinks == null)
            {
                dtMatrix.Rows.Add(linkedIssues.ToArray());
                return;
            }

            JiraIssueType nextIssueTypeInPath = issueTypes[issueTypeIndex].JiraIssueType;

            List<JiraVuIssue> nextSetOfLinkedIssues = new List<JiraVuIssue>();

            // Collect any directly linked issues
            foreach (JiraIssueLink link in issue.JiraIssue.fields.issuelinks)
            {
                // Get the inward or outward linking issue, whichever exists
                JiraIssue linkedIssue = link.inwardissue != null ? link.inwardissue : link.outwardissue;
                // Check if the issue's type is the next one in the trace
                if (linkedIssue.fields.issuetype.name == nextIssueTypeInPath.name)
                {
                    // The Jira REST API does not provide the next level of links, so
                    // Get the full jira issue and its links
                    nextSetOfLinkedIssues.Add(
                        new JiraVuIssue(
                            ParentWindow.RestClient.GetIssueByID(linkedIssue.id), 
                            ParentWindow.RestClient.ServerUrl));
                }
            }

            if (IncludeIndirectLinks)
            {
                // Collect any indirectly linked issues (i.e. an issue's subtask is linked to the issue type in question)
                foreach (JiraIssue subTask in issue.JiraIssue.fields.subtasks)
                {
                    JiraIssue fullSubTask = ParentWindow.RestClient.GetIssueByID(subTask.id);

                    foreach (JiraIssueLink link in fullSubTask.fields.issuelinks)
                    {
                        // Get the inward or outward linking issue, whichever exists
                        JiraIssue linkedIssue = link.inwardissue != null ? link.inwardissue : link.outwardissue;
                        // Check if the issue's type is the next one in the trace
                        if (linkedIssue.fields.issuetype.name == nextIssueTypeInPath.name)
                        {
                            // TODO: Ensure the same issue isn't added multiple times
                            nextSetOfLinkedIssues.Add(
                                new JiraVuIssue(
                                    ParentWindow.RestClient.GetIssueByID(linkedIssue.id),
                                    ParentWindow.RestClient.ServerUrl));
                        }
                    }
                }
            }

            if (nextSetOfLinkedIssues.Count == 0)
            {   // At the end of the trace, so add the collected issues to data table
                dtMatrix.Rows.Add(linkedIssues.ToArray());
            }
            else
            {
                foreach (JiraVuIssue linkedIssue in nextSetOfLinkedIssues)
                {
                    linkedIssues.Add(linkedIssue);
                    AddLinkTraceabilityRow(linkedIssue, ref issueTypes, issueTypeIndex, linkedIssues);
                    linkedIssues.RemoveAt(linkedIssues.Count - 1);
                }
            }
        }

        private void GenerateDataGridColumns(DataTable dataTable)
        {
            // Dynamically add columns to data grid
            foreach (DataColumn clmn in dataTable.Columns)
            {
                // Build the column
                DataGridTemplateColumn gridClmn = new DataGridTemplateColumn();
                // Set the data template for the grid
                gridClmn.CellTemplate = (DataTemplate)FindResource("IssueCard");
                
                //DataGridTextColumn gridClmn = new DataGridTextColumn();
                gridClmn.Header = clmn.ColumnName;

                // Build the binding to display JIRA issue key
                //gridClmn.Binding = new Binding(String.Format("{0}.JiraIssue.key", clmn.ColumnName));

                gridClmn.IsReadOnly = true;
                gridClmn.Width = new DataGridLength(50, DataGridLengthUnitType.Star);
                gridClmn.CanUserSort = false;

                // Build the column header style
                Style headerStyle = new Style();
                headerStyle.Setters.Add(new Setter(HorizontalContentAlignmentProperty, HorizontalAlignment.Center));

                // Build the element style for the column
                Style elemStyle = new Style(typeof(Grid));
                //Style elemStyle = new Style(typeof(TextBlock));
                //elemStyle.Setters.Add(new Setter(HorizontalAlignmentProperty, HorizontalAlignment.Center));
                
                // Build the binding to provide each cell with a toolip
                Binding tooltipBinding = new Binding(String.Format("{0}.JiraIssue.fields.summary", clmn.ColumnName));
                elemStyle.Setters.Add(new Setter(ToolTipProperty, tooltipBinding));

                // Build the cell style for the column
                Style cellStyle = new Style(typeof(DataGridCell));

                // Build the binding to match status with user-selected color
                Binding colorBinding = new Binding(String.Format("{0}.JiraIssue.fields.status.name", clmn.ColumnName));
                colorBinding.Converter = StatusToColorConverter.Instance;
                colorBinding.ConverterParameter = ParentWindow.StatusColors;
                cellStyle.Setters.Add(new Setter(BackgroundProperty, colorBinding));

                // Add styles to the column
                gridClmn.HeaderStyle = headerStyle;
                gridClmn.CellStyle = cellStyle;
                //gridClmn.ElementStyle = elemStyle;

                // Add the column to the datagrid
                dgTraceMatrix.Columns.Add(gridClmn);
            }
        }

        private void GenerateTraceabilityMatrix()
        {
            if (!_initialRun) UpdateSelectedIssues();
            _initialRun = false;

            List<JiraVuIssueType> selectedIssueTypes = ParentWindow.SelectedIssueTypes;

            dtMatrix = new DataTable();
            string clmnName;
            string issueTypeName;
            Dictionary<string, int> dictTypeCounts = new Dictionary<string, int>();

            // Add columns to data table based on selected issue types
            foreach (JiraVuIssueType issueType in selectedIssueTypes)
            {
                issueTypeName = issueType.JiraIssueType.name;
                if (!dictTypeCounts.ContainsKey(issueTypeName))
                {
                    dictTypeCounts.Add(issueTypeName, 1);
                }
                clmnName = !dtMatrix.Columns.Contains(issueTypeName)
                                ? issueTypeName
                                : issueTypeName + dictTypeCounts[issueTypeName]++;
                dtMatrix.Columns.Add(clmnName, typeof(JiraVuIssue));
            }

            // Begin populating the data table with issues matching first issue type
            JiraVuIssueType firstType = ParentWindow.SelectedIssueTypes[0];
            foreach (JiraVuIssue issue in ParentWindow.SelectedIssues)
            {
                if (issue.JiraIssue.fields.issuetype.name == firstType.JiraIssueType.name)
                {
                    List<JiraVuIssue> linkedIssues = new List<JiraVuIssue>();
                    linkedIssues.Add(issue);
                    AddLinkTraceabilityRow(issue, ref selectedIssueTypes, 0, linkedIssues);
                }
            }

            dgTraceMatrix.Columns.Clear();
            // Set data source for the data grid
            dgTraceMatrix.ItemsSource = dtMatrix.DefaultView;

            GenerateDataGridColumns(dtMatrix);

            tbRowCount.Text = dgTraceMatrix.Items.Count.ToString();
        }

        private void UpdateSelectedIssues()
        {
            try
            {
                BackgroundWorker worker = new BackgroundWorker();
                worker.DoWork += (o, ea) =>
                {
                    string jql = ParentWindow.CurrentJqlSearch.ToLower();
                    // Build Jql based on the keys of the issues selected by user
                    StringBuilder sb = new StringBuilder();
                    sb.Append("issuekey in (");
                    bool addComma = false;
                    foreach (JiraVuIssue selIssue in ParentWindow.SelectedIssues)
                    {
                        if (addComma) sb.Append(", ");
                        addComma = true;
                        sb.Append("\"").Append(selIssue.JiraIssue.key).Append("\"");
                    }
                    sb.Append(")");

                    // Append the sort order, if provided
                    if (jql.Contains("order by") && (jql.Contains("asc") || jql.Contains("desc")))
                    {   
                        sb.Append(jql.Substring(jql.LastIndexOf("order by") - 1));
                    }

                    // Get the latest information for the selected issues
                    List<JiraIssue> jiraIssuesList = new List<JiraIssue>();
                    // Get the list of issues matching the jql
                    Dispatcher.Invoke((Action)(() =>
                        jiraIssuesList = ParentWindow.RestClient.GetIssuesByJqlSearch(
                            sb.ToString(), 
                            0,
                            Properties.Settings.Default.MaxResultsInt)));
                    // Convert JiraIssue list into JiraVuIssue list
                    ParentWindow.SelectedIssues = jiraIssuesList.ConvertAll<JiraVuIssue>(
                        delegate(JiraIssue i)
                        {
                            return new JiraVuIssue(i, ParentWindow.RestClient.ServerUrl);
                        });
                };
                worker.RunWorkerCompleted += (o, ea) =>
                {
                    //FIXME: ParentWindow.ProgressBar.Visibility = System.Windows.Visibility.Collapsed;
                };
                //FIXME: ParentWindow.ProgressBar.Visibility = System.Windows.Visibility.Visible;
                worker.RunWorkerAsync();
            }
            catch (Exception) { }
        }

        #endregion

        #region Public Methods

        public bool DoWorkAsync()
        {
            return true;
        }

        public bool PrepMoveBack()
        {
            // Prepare for hand-off to the previous user control
            ((INavigationUserControl)BackUserControl).NextUserControl = this;
            return ((INavigationUserControl)BackUserControl).UpdateParentUI();
        }

        public bool PrepMoveNext()
        {
            // Prepare for hand-off to the next user control
            ((INavigationUserControl)NextUserControl).BackUserControl = this;
            return ((INavigationUserControl)NextUserControl).UpdateParentUI();
        }

        public bool UpdateParentUI()
        {
            ParentWindow.BackButton.IsEnabled = true;
            ParentWindow.NextButton.IsEnabled = true;
            return true;
        }

        #endregion
        
        #region Event Handlers

        private void btnGenerateTraceabilityMatrix_Click(object sender, RoutedEventArgs e)
        {
            GenerateTraceabilityMatrix();
        }

        private void chkIncludeIndirectLinks_Checked(object sender, RoutedEventArgs e)
        {
            IncludeIndirectLinks = chkIncludeIndirectLinks.IsChecked.Value;
        }

        private void dgTraceMatrix_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            int clmnIndex = ((DataGrid)sender).SelectedCells[0].Column.DisplayIndex;
            JiraVuIssue issueClicked = (JiraVuIssue)((DataRowView)((DataGrid)sender).SelectedCells[0].Item).Row[clmnIndex];

            Hyperlink link = new Hyperlink();
            link.NavigateUri = issueClicked.IssueUri;
            System.Diagnostics.Process.Start(link.NavigateUri.AbsoluteUri);
        }

        #endregion

    }
}
