﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace JiraVu.Loggers
{
    public static class LogHelper
    {
        private static string _appLogName = "app_log.txt";
        private static string _execDirectory = String.Empty;
        private static FileStream _appLogFileStream;

        public static bool AppLogExists
        {
            get
            {
                return File.Exists(Path.Combine(ExecutionDirectory, _appLogName));
            }
        }

        public static FileStream AppLogFileStream
        {
            get
            {
                if (_appLogFileStream == null)
                    _appLogFileStream = File.OpenWrite(Path.Combine(ExecutionDirectory, _appLogName));
                return _appLogFileStream;
            }
        }

        public static string ExecutionDirectory
        {
            get
            {
                if (_execDirectory == String.Empty)
                    _execDirectory = System.IO.Path.GetDirectoryName(
                        System.Reflection.Assembly.GetExecutingAssembly().Location);
                return _execDirectory;
            }
        }

        //public static async void CleanAppLog()
        //{
            //string fileName = Path.Combine(ExecutionDirectory, _appLogName);
            //int oldEntryDate = Convert.ToInt32(GetOldLogEntryPrefix());
            //String result;
            //using (StreamReader reader = File.OpenText(fileName))
            //{
            //    result = await reader.ReadLineAsync();
            //    result = result.Substring(1, result.IndexOf(']', 1));
                
            //}

            //using (StreamReader reader = new StreamReader("C:\\input"))
            //{
            //    using (StreamWriter writer = new StreamWriter("C:\\output"))
            //    {
            //        while ((line = reader.ReadLine()) != null)
            //        {
            //            if (String.Compare(line, line_to_delete) == 0)
            //                continue;

            //            writer.WriteLine(line);
            //        }
            //    }
            //}
        //}

        public static async void LogException(Exception ex)
        {
            string fileName = Path.Combine(ExecutionDirectory, _appLogName);
            byte[] msg = GetBytes(GetLogEntryPrefix() + ex.Message + Environment.NewLine);

            using (FileStream fs = new FileStream(fileName, FileMode.Append))
            {
                await fs.WriteAsync(msg, 0, msg.Length);
                await fs.FlushAsync();
            }
        }

        public static async void LogMessage(string message)
        {
            string fileName = Path.Combine(ExecutionDirectory, _appLogName);
            byte[] msg = GetBytes(GetLogEntryPrefix() + message + Environment.NewLine);

            using (FileStream fs = new FileStream(fileName, FileMode.Append))
            {
                await fs.WriteAsync(msg, 0, msg.Length);
                await fs.FlushAsync();
            }
        }

        private static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        private static string GetLogEntryPrefix()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("[").Append(DateTime.Now.Year).Append(DateTime.Now.Month).Append(DateTime.Now.Day).Append("-").
                Append(DateTime.Now.TimeOfDay).Append("] ");
            return sb.ToString();
        }

        private static string GetOldLogEntryPrefix()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(DateTime.Now.Year).Append(DateTime.Now.Month).Append(DateTime.Now.Day - 7);
            return sb.ToString();
        }
    }
}
