﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using JiraRestClient;

namespace JiraVu.Core
{
    public class LinkedIssuesButton : Button, INotifyPropertyChanged
    {
        private int linkedIssuesCount;
        private JiraVuIssue sourceJiraVuIssue;
        private JiraIssueType jiraIssueType;

        #region Public Properties

        public JiraIssueType JiraIssueType
        {
            get { return jiraIssueType; }
            set { jiraIssueType = value; RaisePropertyChanged("JiraIssueType"); }
        }

        public int LinkedIssuesCount
        {
            get { return linkedIssuesCount; }
            set { linkedIssuesCount = value; RaisePropertyChanged("LinkedIssuesCount"); }
        }

        public JiraVuIssue SourceJiraVuIssue
        {
            get { return sourceJiraVuIssue; }
            set { sourceJiraVuIssue = value; RaisePropertyChanged("SourceJiraVuIssue"); }
        }

        #endregion Public Properties

        #region Constructors

        public LinkedIssuesButton() { }

        public LinkedIssuesButton(JiraVuIssue sourceIssue) 
        {
            SourceJiraVuIssue = sourceIssue;
        }

        public LinkedIssuesButton(JiraVuIssue sourceIssue, JiraIssueType jiraIssueType)
            : this(sourceIssue)
        {
            JiraIssueType = jiraIssueType;
        }

        public LinkedIssuesButton(JiraVuIssue sourceIssue, JiraIssueType jiraIssueType, int linkedIssuesCount)
            : this(sourceIssue, jiraIssueType)
        {
            LinkedIssuesCount = linkedIssuesCount;
        }

        #endregion Constructors

        public event PropertyChangedEventHandler PropertyChanged;

        public void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
