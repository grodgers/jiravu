﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using JiraVu.Core;
using JiraRestClient;

namespace JiraVu.Core
{
    public class FilterFolder : JiraVuBase, IEquatable<FilterFolder>, INode
    {
        private bool hasJql = false;
        private bool hasParent = false;
        private bool isExpanded = false;
        private bool isHoveredOver = false;
        private bool isSelected = false;
        private Guid id;
        private string jql;
        private string name;
        private int descendentIssuesCount = 0;
        private int directChildIssuesCount = 0;
        private ObservableCollection<INode> children;

        #region Public Properties

        public bool HasJql
        {
            get { return hasJql; }
            set { hasJql = value; RaisePropertyChanged("HasJql"); }
        }

        public bool HasParent
        {
            get { return hasParent; }
            set { hasParent = value; RaisePropertyChanged("HasParent"); }
        }

        public bool IsExpanded
        {
            get { return isExpanded; }
            set { isExpanded = value; RaisePropertyChanged("IsExpanded"); }
        }

        public bool IsHoveredOver
        {
            get { return isHoveredOver; }
            set { isHoveredOver = value; RaisePropertyChanged("IsHoveredOver"); }
        }

        public bool IsFirstChild
        {
            get
            {
                if (ParentFolder == null) return true;
                return (ParentFolder.Children.IndexOf(this) == 0);
            }
            set { RaisePropertyChanged("IsFirstChild"); }
        }

        public bool IsLastChild
        {
            get
            {
                if (ParentFolder == null) return true;
                return (ParentFolder.Children.IndexOf(this) == ParentFolder.Children.Count - 1);
            }
            set { RaisePropertyChanged("IsLastChild"); }
        }

        public bool IsSelected
        {
            get { return isSelected; }
            set
            {
                if (value != isSelected)
                {
                    isSelected = value; RaisePropertyChanged("IsSelected");
                }
            }
        }

        public Guid Id
        {
            get { return id; }
        }

        public FilterFolder ParentFolder { get; set; }

        public int DescendentIssuesCount
        {
            get { return descendentIssuesCount; }
            set { descendentIssuesCount = value; RaisePropertyChanged("DescendentIssuesCount"); }
        }

        public int DirectChildIssuesCount
        {
            get { return directChildIssuesCount; }
            set { directChildIssuesCount = value; RaisePropertyChanged("DirectChildIssuesCount"); }
        }
        
        public ObservableCollection<ChildrenStatusIdicator> ChildrenStatusCollection
        {
            get
            {
                ObservableCollection<ChildrenStatusIdicator> statusColl = new ObservableCollection<ChildrenStatusIdicator>();
                Dictionary<string, List<JiraVuIssue>> dictIssues = new Dictionary<string, List<JiraVuIssue>>();

                foreach (INode node in Children)
                {
                    if (node is JiraVuIssue)
                    {
                        JiraVuIssue issue = node as JiraVuIssue;

                        if (!dictIssues.ContainsKey(issue.JiraIssue.fields.status.name))
                        {   // Add the issue to a new collection
                            dictIssues.Add(
                                issue.JiraIssue.fields.status.name, 
                                new List<JiraVuIssue>() { issue });
                        }
                        else
                        {   // Add the issue to an existing collection
                            dictIssues[issue.JiraIssue.fields.status.name].Add(issue);
                        }
                    }
                }

                foreach (string key in dictIssues.Keys)
                {
                    statusColl.Add(
                        new ChildrenStatusIdicator() 
                        { 
                            JiraStatus = dictIssues[key][0].JiraIssue.fields.status,
                            IssueCount = dictIssues[key].Count
                        });
                }

                return statusColl;
            }
        }

        public ObservableCollection<INode> Children
        {
            get
            {
                if (children == null)
                    children = new ObservableCollection<INode>();
                return children;
            }
            set 
            {
                children = value; 
                RaisePropertyChanged("Children"); 
                RaisePropertyChanged("ChildrenStatusCollection");
            }
        }

        public string JiraJql
        {
            get
            {
                if (jql == null)
                    jql = Properties.Settings.Default.DefaultJqlMessage;
                return jql;
            }
            set
            {
                jql = value;
                if (jql != Properties.Settings.Default.DefaultJqlMessage && jql != String.Empty)
                    HasJql = true;
                else
                    HasJql = false;
                RaisePropertyChanged("JiraJql");
            }
        }

        public string Name
        {
            get { return name; }
            set { name = value; RaisePropertyChanged("Name"); }
        }

        #endregion

        #region Constructors

        public FilterFolder(string name)
        {
            id = Guid.NewGuid();
            Name = name;
        }

        public FilterFolder(string name, FilterFolder parentFolder)
            : this(name)
        {
            ParentFolder = parentFolder;
            HasParent = true;
        }

        public FilterFolder(string name, FilterFolder parentFolder, string jql)
            : this(name, parentFolder)
        {
            JiraJql = jql;
            HasJql = true;
        }

        #endregion

        #region Public Methods

        public override bool Equals(object obj)
        {
            FilterFolder fldr = obj as FilterFolder;
            if (fldr != null)
            {
                return this.Id == fldr.Id;
            }
            else
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }

        public void DeleteChild(FilterFolder f)
        {
            Children.Remove(f);
        }

        public void MoveChildDown(FilterFolder f)
        {
            int currentIndex = GetChildIndex(f);
            int newIndex = currentIndex++;
            Children.Move(currentIndex, newIndex);
            if (newIndex == Children.Count)
                f.IsLastChild = true;
        }

        public void MoveChildUp(FilterFolder f)
        {
            int currentIndex = GetChildIndex(f);
            int newIndex = currentIndex--;
            Children.Move(currentIndex, newIndex);
            if (newIndex == 0)
                f.IsFirstChild = true;
        }

        public void UpdateChildIssueCount()
        {
            int descIssueCount = 0;
            int directIssueCount = 0;

            foreach (INode child in Children)
            {
                if (child is JiraVuIssue) directIssueCount++;
                else if (child is FilterFolder)
                {
                    GetDescendentIssuesCount(child as FilterFolder, ref descIssueCount);
                }
            }

            DirectChildIssuesCount = directIssueCount;
            DescendentIssuesCount = descIssueCount + directIssueCount;
        }

        #endregion Public Methods

        #region Private Methods

        bool IEquatable<FilterFolder>.Equals(FilterFolder other)
        {
            if (other != null)
                return this.Id == other.Id;

            throw new ArgumentException("Object being compared is null.");
        }

        bool IEquatable<INode>.Equals(INode other)
        {
            if (other == null)
                throw new ArgumentException("Object being compared is null.");

            if (other is FilterFolder)
            {
                FilterFolder fldr = other as FilterFolder;
                return this.Id == fldr.Id;
            }

            return false;
        }

        private int GetChildIndex(FilterFolder f)
        {
           for (int ind = 0; ind < Children.Count; ind++)
                if (Children[ind] == f) return ind;

            return -1;
        }

        private void GetDescendentIssuesCount(FilterFolder f, ref int count)
        {
            foreach (INode child in f.Children)
            {
                if (child is JiraVuIssue) count++;
                else if (child is FilterFolder)
                {
                    GetDescendentIssuesCount(child as FilterFolder, ref count);
                }
            }
        }

        #endregion

    }

    public class ChildrenStatusIdicator
    {
        public JiraStatus JiraStatus { get; set; }
        public int IssueCount { get; set; }
    }
}
