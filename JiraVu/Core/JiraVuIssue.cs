﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using JiraRestClient;
using JiraVu.Core;
using System.Collections.ObjectModel;
using System.Collections;

namespace JiraVu.Core
{
    public class JiraVuIssue : JiraVuBase, IComparable, IEquatable<JiraVuIssue>, INode
    {
        private bool isSelected = false;
        private Enums.OperationStates currentState = Enums.OperationStates.Default;
        private ObservableCollection<JiraIssue> linkedIssues;
        private ObservableCollection<LinkedIssuesButton> linkedIssuesButtons;
        private ObservableCollection<JiraIssue> _subTasks;
        private ObservableCollection<JiraIssue> _subTasksAndLinkedIssues;
        private ObservableCollection<SubtaskIssuesButton> _subTasksButtons;
        private string name = "";
        private Uri issueUri;
        private JiraIssue jiraIssue;

        #region Public Properties

        /// <summary>
        /// Whether or not the issue has been selected to be included in an operation.
        /// </summary>
        public bool IsSelected
        {
            get { return this.isSelected; }
            set
            {
                if (value != this.isSelected)
                {
                    isSelected = value;
                    RaisePropertyChanged("IsSelected");
                }
            }
        }

        /// <summary>
        /// The current state of this issue during a bulk operation.
        /// </summary>
        public Enums.OperationStates OperationState
        {
            get { return this.currentState; }
            set
            {
                if (value != this.currentState)
                {
                    this.currentState = value;
                    RaisePropertyChanged("OperationState");
                }
            }
        }

        public FilterFolder ParentFolder { get; set; }

        /// <summary>
        /// The base class of JiraIssue
        /// </summary>
        public JiraIssue JiraIssue
        {
            get { return jiraIssue; }
            set { jiraIssue = value; RaisePropertyChanged("JiraIssue"); }
        }

        public ObservableCollection<JiraIssue> LinkedIssues
        {
            get
            {
                if (linkedIssues == null) 
                    GetLinkedIssues();
                return linkedIssues;
            }
            set { linkedIssues = value; RaisePropertyChanged("LinkedIssues"); }
        }

        public ObservableCollection<LinkedIssuesButton> LinkedIssuesButtons
        {
            get
            {
                if (linkedIssuesButtons == null) 
                    GetLinkedIssueButton();
                return linkedIssuesButtons;
            }
            set { linkedIssuesButtons = value; RaisePropertyChanged("LinkedIssueButton"); }
        }

        public ObservableCollection<JiraIssue> SubtaskIssues
        {
            get
            {
                if (_subTasks == null)
                    GetSubtasks();
                return _subTasks;
            }
            set { _subTasks = value; RaisePropertyChanged("SubtaskIssues"); }
        }

        public ObservableCollection<SubtaskIssuesButton> SubtaskButtons
        {
            get
            {
                if (_subTasksButtons == null)
                    GetSubtaskButtons();
                return _subTasksButtons;
            }
            set { _subTasksButtons = value; RaisePropertyChanged("SubtaskButtons"); }
        }

        public ObservableCollection<JiraIssue> SubtasksAndLinkedIssues
        {
            get
            {
                if (_subTasksAndLinkedIssues == null)
                    GetSubtasksAndLinkedIssues();
                return _subTasksAndLinkedIssues;
            }
            set { _subTasksAndLinkedIssues = value; RaisePropertyChanged("SubtasksAndLinkedIssues"); }
        }

        public string Name
        {
            get
            {
                if (JiraIssue != null)
                    name = JiraIssue.key;
                return name;
            }
            set
            {
                name = value;
            }
        }
        
        /// <summary>
        /// The URI for the issue
        /// </summary>
        public Uri IssueUri
        {
            get { return issueUri; }
            set
            {
                if (value != issueUri)
                {
                    issueUri = value;
                    RaisePropertyChanged("IssueUri");
                }
            }
        }

        #endregion

        #region Constructors

        public JiraVuIssue(JiraIssue jiraIssue, string restBaseUrl)
        {
            this.JiraIssue = jiraIssue;
            this.IssueUri = new Uri(String.Format("{0}/browse/{1}", restBaseUrl, jiraIssue.key));
        }

        public JiraVuIssue(JiraIssue jiraIssue, string restBaseUrl, FilterFolder parentFolder)
            : this(jiraIssue, restBaseUrl)
        {
            this.ParentFolder = parentFolder;
        }

        #endregion Constructors

        #region Private Methods

        bool IEquatable<JiraVuIssue>.Equals(JiraVuIssue other)
        {
            if (other != null)
                return Name == other.Name;

            throw new ArgumentException("Object being compared is null.");
        }

        bool IEquatable<INode>.Equals(INode other)
        {
            if (other is JiraVuIssue)
            {
                JiraVuIssue temp = (JiraVuIssue)other;
                return Name == temp.Name;
            }

            return false;
        }

        #endregion Private Methods

        #region Public Methods

        public int CompareTo(object obj)
        {
            if (obj is JiraVuIssue)
            {
                JiraVuIssue temp = (JiraVuIssue)obj;
                return name.CompareTo(temp.name);
            }

            throw new ArgumentException("Object being compared is not of type JiraVuIssue.");
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            JiraVuIssue iss = obj as JiraVuIssue;
            if (iss != null)
            {
                return JiraIssue.key.Equals(iss.JiraIssue.key);
            }
            else
            {
                return false;
            }
        }

        public void GetLinkedIssues()
        {
            linkedIssues = new ObservableCollection<JiraIssue>();

            if (JiraIssue != null)
            {
                foreach (JiraIssueLink il in JiraIssue.fields.issuelinks)
                {
                    JiraIssue ji = (il.inwardissue == null)
                        ? il.outwardissue
                        : il.inwardissue;

                    linkedIssues.Add(ji);
                }
            }
        }

        public void GetLinkedIssueButton()
        {
            linkedIssuesButtons = new ObservableCollection<LinkedIssuesButton>();
            Dictionary<string, ObservableCollection<JiraIssue>> dictIssues = new Dictionary<string, ObservableCollection<JiraIssue>>();
            Dictionary<string, JiraIssueType> dictIssueTypes = new Dictionary<string, JiraIssueType>();

            // Collect linked issues by their issue type
            foreach (JiraIssue ji in LinkedIssues)
            {
                if (!dictIssueTypes.ContainsKey(ji.fields.issuetype.name))
                {   // Store the JiraIssueType
                    dictIssueTypes.Add(
                        ji.fields.issuetype.name,
                        ji.fields.issuetype);
                    // Store the JiraIssue in the collection related to the issue type
                    dictIssues.Add(
                        ji.fields.issuetype.name,
                        new ObservableCollection<JiraIssue>() { ji });
                }
                else
                {   // Store the JiraIssue in the collection related to the issue type
                    dictIssues[ji.fields.issuetype.name].Add(ji);
                }
            }

            // Create a LinkedIssuesButton for each issue type linked
            foreach (string k in dictIssueTypes.Keys)
            {   // Create the button for the issue type
                LinkedIssuesButton btn = new LinkedIssuesButton(
                    this, dictIssueTypes[k], dictIssues[k].Count);
                // Add the button to the list
                linkedIssuesButtons.Add(btn);
            }
        }

        public void GetSubtasks()
        {
            _subTasks = new ObservableCollection<JiraIssue>();

            if (JiraIssue != null)
            {
                foreach (JiraIssue i in JiraIssue.fields.subtasks)
                {
                    _subTasks.Add(i);
                }
            }
        }

        public void GetSubtaskButtons()
        {
            _subTasksButtons = new ObservableCollection<SubtaskIssuesButton>();
            Dictionary<string, ObservableCollection<JiraIssue>> dictIssues = new Dictionary<string, ObservableCollection<JiraIssue>>();
            Dictionary<string, JiraIssueType> dictIssueTypes = new Dictionary<string, JiraIssueType>();

            // Collect sub-tasks by their issue type
            foreach (JiraIssue ji in JiraIssue.fields.subtasks)
            {
                if (!dictIssueTypes.ContainsKey(ji.fields.issuetype.name))
                {   // Store the JiraIssueType
                    dictIssueTypes.Add(
                        ji.fields.issuetype.name,
                        ji.fields.issuetype);
                    // Store the JiraIssue in the collection related to the issue type
                    dictIssues.Add(
                        ji.fields.issuetype.name,
                        new ObservableCollection<JiraIssue>() { ji });
                }
                else
                {   // Store the JiraIssue in the collection related to the issue type
                    dictIssues[ji.fields.issuetype.name].Add(ji);
                }
            }

            // Create a SubtaskIssuesButton for each issue type found
            foreach (string k in dictIssueTypes.Keys)
            {   // Create the button for the issue type
                SubtaskIssuesButton btn = new SubtaskIssuesButton(
                    this, dictIssueTypes[k], dictIssues[k].Count);
                // Add the button to the list
                _subTasksButtons.Add(btn);
            }
        }

        public void GetSubtasksAndLinkedIssues()
        {
            _subTasksAndLinkedIssues = new ObservableCollection<JiraIssue>();

            // Start by adding all sub-tasks to the list
            _subTasksAndLinkedIssues = SubtaskIssues;

            // Then add linked issues to the same list
            foreach (JiraIssue i in LinkedIssues)
            {
                _subTasksAndLinkedIssues.Add(i);
            }
        }

        #endregion Public Methods
    }
}