﻿using System;
using System.Windows.Controls;

namespace JiraVu.Core
{
    public class JiraVuListBoxItem : ListBoxItem
    {
        public string DisplayText { get; set; }
        public string ItemValue { get; set; }

        public JiraVuListBoxItem() { }

        public JiraVuListBoxItem(string displayText, string itemValue)
        {
            this.DisplayText = displayText;
            base.Content = DisplayText;
            this.ItemValue = itemValue;
        }

        public override string ToString()
        {
            return DisplayText;
        }
    }
}
