﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace JiraVu.Core
{
    public interface INavigationUserControl
    {
        UserControl BackUserControl { get; set; }
        UserControl NextUserControl { get; set; }
        /// <summary>
        /// A reference to the parent window containing this control.
        /// </summary>
        MainWindow ParentWindow { get; set; }
        bool DoWorkAsync();
        /// <summary>
        /// Prepare the next UserControl in the workflow to display correctly in the parent window.
        /// </summary>
        /// <returns>True, if preparation was successful. Otherwise, false.</returns>
        bool PrepMoveNext();
        /// <summary>
        /// Prepare the previous UserControl in the workflow to display correctly in the parent window.
        /// </summary>
        /// <returns>True, if preparation was successful. Otherwise, false.</returns>
        bool PrepMoveBack();
        /// <summary>
        /// Update the parent window UI, as appropriate for the current UserControl.
        /// </summary>
        /// <returns>True, if the update was successful. Otherwise, false.</returns>
        bool UpdateParentUI();
    }
}
