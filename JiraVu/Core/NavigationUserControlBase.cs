﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using JiraVu.Views;

namespace JiraVu.Core
{
    public class NavigationUserControlBase : JiraVuUserControlBase, INavigationUserControl
    {
        private bool _isBusy = false;

        #region Constructors

        public NavigationUserControlBase() {}

        #endregion

        #region Public Properties

        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                _isBusy = value;
                RaisePropertyChanged("IsBusy");
            }
        }

        #endregion

        public UserControl BackUserControl { get; set; }

        public UserControl NextUserControl { get; set; }

        public MainWindow ParentWindow { get; set; }

        public bool DoWorkAsync()
        {
            return true;
        }

        public bool PrepMoveBack()
        {
            return true;
        }

        public bool PrepMoveNext()
        {
            return true;
        }

        public bool UpdateParentUI()
        {
            return true;
        }
    }
}
