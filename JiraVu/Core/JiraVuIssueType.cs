﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using JiraRestClient;
using JiraVu.Core;

namespace JiraVu.Core
{
    public class JiraVuIssueType : JiraVuBase
    {
        #region Enums

        public enum IssueTypeClasses
        {
            Standard,
            Subtask,
            All
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// The order of the issue type within a trace definition
        /// </summary>
        public int TraceIndex { get; set; }

        /// <summary>
        /// The project to which this issue type information pertains
        /// </summary>
        public JiraProject Project { get; set; }

        /// <summary>
        /// The base class of JiraIssueType
        /// </summary>
        public JiraIssueType JiraIssueType { get; set; }

        /// <summary>
        /// Collection of field names and associated metadata
        /// </summary>
        public List<JiraVuIssueTypeFieldMeta> Fields { get; set; }

        #endregion

        #region Constructors

        public JiraVuIssueType(JiraIssueType jiraIssueType) : this(null, jiraIssueType) { }

        public JiraVuIssueType(JiraProject jiraProject, JiraIssueType jiraIssueType)
        {
            this.Project = jiraProject;
            this.JiraIssueType = jiraIssueType;
            this.Fields = new List<JiraVuIssueTypeFieldMeta>();
            if (jiraIssueType.fields != null)
            {
                foreach (string key in jiraIssueType.fields.Keys)
                {
                    this.Fields.Add(new JiraVuIssueTypeFieldMeta(jiraIssueType.fields[key].name, jiraIssueType.fields[key]));
                }
            }
        }

        #endregion
    }
}
