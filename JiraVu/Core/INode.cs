﻿using System;
using System.Collections.ObjectModel;

namespace JiraVu.Core
{
    public interface INode : IEquatable<INode>
    {
        string Name { get; set; }
        bool IsSelected { get; set; }
        FilterFolder ParentFolder { get; set; }
    }
}
