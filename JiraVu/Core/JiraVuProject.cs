﻿using System;
using System.Collections.Generic;
using JiraRestClient;

namespace JiraVu.Core
{
    public class JiraVuProject
    {
        #region Public Properties

        public JiraProject JiraProject { get; set; }

        public List<JiraVuIssueType> IssueTypes { get; set; }

        #endregion

        #region Constructors

        public JiraVuProject() : this(null, true) { }

        public JiraVuProject(JiraProject jiraProject) : this(jiraProject, true) { }

        public JiraVuProject(JiraProject jiraProject, bool copyIssueTypes)
        {   // Initialize the issue types list
            this.IssueTypes = new List<JiraVuIssueType>();

            if (copyIssueTypes)
            {   // Copy everything over
                this.JiraProject = jiraProject;
            }
            else
            {   // Copy everything except the issuetypes
                this.JiraProject = new JiraProject();
                this.JiraProject.avatarUrls = jiraProject.avatarUrls;
                this.JiraProject.id = jiraProject.id;
                this.JiraProject.key = jiraProject.key;
                this.JiraProject.name = jiraProject.name;
                this.JiraProject.self = jiraProject.self;
            }
        }

        #endregion
    }
}
