﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JiraRestClient;

namespace JiraVu.Core
{
    public class JiraVuIssueTypeFieldMeta : JiraIssueTypeFieldMeta, INotifyPropertyChanged
    {
        private bool isSelected = false;
        private bool isValueSupplied = false;

        #region Public Properties

        /// <summary>
        /// Whether or not the issue type field has been selected to be included in an operation.
        /// </summary>
        public bool IsSelected
        {
            get { return this.isSelected; }
            set
            {
                if (value != this.isSelected)
                {
                    this.isSelected = value;
                    NotifyPropertyChanged("IsSelected");
                }
            }
        }

        /// <summary>
        /// Whether or not the issue type field has been supplied a value from the user to be used in an operation.
        /// </summary>
        public bool IsValueSupplied
        {
            get { return this.isValueSupplied; }
            set
            {
                if (value != this.isValueSupplied)
                {
                    this.isValueSupplied = value;
                    NotifyPropertyChanged("IsValueSupplied");
                }
            }
        }

        /// <summary>
        /// The base class of JiraIssueTypeFieldMeta
        /// </summary>
        public JiraIssueTypeFieldMeta JiraIssueTypeFieldMeta { get; set; }

        public string FieldName { get; set; }
        public string FieldValue { get; set; }

        #endregion

        #region Constructors

        public JiraVuIssueTypeFieldMeta(string fieldName, JiraIssueTypeFieldMeta jiraIssueTypeFieldMeta)
        {
            this.FieldName = fieldName;
            this.JiraIssueTypeFieldMeta = jiraIssueTypeFieldMeta;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Notify clients, typically binding clients, that a property value has changed.
        /// </summary>
        /// <param name="info">The property that has changed</param>
        private void NotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(info));
        }

        #endregion

        #region Event Handlers

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
}