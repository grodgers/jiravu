﻿using System;
using System.ComponentModel;
using JiraRestClient;

namespace JiraVu.Core
{
    public class JiraVuStatusColor : INotifyPropertyChanged
    {
        private System.Windows.Media.Color _color;

        public JiraStatus Status { get; set; }
        public System.Windows.Media.Color Color
        {
            get { return _color; }
            set
            {
                if (value != _color)
                {
                    _color = value;
                    NotifyPropertyChanged("Color");
                }
            }
        }

        public JiraVuStatusColor(JiraStatus status)
        {
            this.Status = status;
        }
                
        #region Private Methods

        /// <summary>
        /// Notify clients, typically binding clients, that a property value has changed.
        /// </summary>
        /// <param name="info">The property that has changed</param>
        private void NotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(info));
        }

        #endregion

        #region Event Handlers

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
}
