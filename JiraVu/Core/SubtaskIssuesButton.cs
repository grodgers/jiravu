﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using JiraRestClient;

namespace JiraVu.Core
{
    public class SubtaskIssuesButton : Button, INotifyPropertyChanged
    {
        private int _subTaskCount;
        private JiraVuIssue sourceJiraVuIssue;
        private JiraIssueType jiraIssueType;
        
        #region Public Properties

        public JiraIssueType JiraIssueType
        {
            get { return jiraIssueType; }
            set { jiraIssueType = value; RaisePropertyChanged("JiraIssueType"); }
        }

        public int SubtaskIssuesCount
        {
            get { return _subTaskCount; }
            set { _subTaskCount = value; RaisePropertyChanged("SubtaskIssuesCount"); }
        }

        public JiraVuIssue SourceJiraVuIssue
        {
            get { return sourceJiraVuIssue; }
            set { sourceJiraVuIssue = value; RaisePropertyChanged("SourceJiraVuIssue"); }
        }

        #endregion Public Properties

        #region Constructors

        public SubtaskIssuesButton() { }

        public SubtaskIssuesButton(JiraVuIssue sourceIssue) 
        {
            SourceJiraVuIssue = sourceIssue;
        }

        public SubtaskIssuesButton(JiraVuIssue sourceIssue, JiraIssueType jiraIssueType)
            : this(sourceIssue)
        {
            JiraIssueType = jiraIssueType;
        }

        public SubtaskIssuesButton(JiraVuIssue sourceIssue, JiraIssueType jiraIssueType, int subtaskCount)
            : this(sourceIssue, jiraIssueType)
        {
            SubtaskIssuesCount = subtaskCount;
        }

        #endregion Constructors

        public event PropertyChangedEventHandler PropertyChanged;

        public void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
