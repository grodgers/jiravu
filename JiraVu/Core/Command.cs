﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace JiraVu.Core
{
    public class Command<T> : ICommand where T : class
    {
        public Action<T> Action { get; set; }

        public Command(Action<T> action)
        {
            Action = action;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged;

        public void Execute(object parameter)
        {
            Action(SafeCast<T>(parameter));
        }

        public static TYPE SafeCast<TYPE>(object obj)
        {
            if (obj != null && obj.GetType().IsAssignableFrom(typeof(TYPE)))
                return (TYPE)obj;
            else
                return default(TYPE);
        }
    }
}
