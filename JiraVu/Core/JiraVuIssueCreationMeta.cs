﻿using System;
using System.Collections.Generic;
using JiraRestClient;

namespace JiraVu.Core
{
    public class JiraVuIssueCreationMeta
    {
        #region Public Properties

        public List<JiraVuProject> Projects { get; set; }

        #endregion

        #region Public Methods

        public JiraVuProject GetProject(string projectKey)
        {
            foreach (JiraVuProject proj in Projects)
            {
                if (proj.JiraProject.key == projectKey) return proj;
            }

            return null;
        }

        public JiraVuIssueType GetIssueTypeForProject(string projectKey, string issueTypeName)
        {
            foreach (JiraVuProject project in Projects)
            {
                if (project.JiraProject.key == projectKey)
                {
                    foreach (JiraVuIssueType issueType in project.IssueTypes)
                    {
                        if (issueType.JiraIssueType.name == issueTypeName)
                        {
                            return issueType;
                        }
                    }
                }
            }

            return null;
        }

        #endregion

        #region Constructors

        public JiraVuIssueCreationMeta()
        {
            this.Projects = new List<JiraVuProject>();
        }

        #endregion
    }
}
