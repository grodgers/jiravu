﻿using System;
using System.Collections.Generic;

namespace JiraRestClient
{
    public class JiraProject : IComparable
    {
        public string self { get; set; }
        public string id { get; set; }
        public string key { get; set; }
        public string name { get; set; }
        public Dictionary<string, string> avatarUrls { get; set; }
        public List<JiraIssueType> issuetypes { get; set; }

        public JiraProject() { }

        public JiraProject(string projectKey)
        {
            this.key = projectKey;
        }

        public int CompareTo(object obj)
        {
            try
            {
                JiraProject toCompare = (JiraProject)obj;
                if (this.id == toCompare.id || this.key == toCompare.key) return 1;
            }
            catch (Exception) { }
            return 0;
        }

        public override bool Equals(object obj)
        {
            try
            {
                JiraProject toCompare = (JiraProject)obj;
                if (this.id == toCompare.id || this.key == toCompare.key) return true;
            } 
            catch (Exception) {}
            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return this.name;
        }
    }
}
