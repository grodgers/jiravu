﻿using System;
using System.Collections.Generic;

namespace JiraRestClient
{
    public class JiraComment
    {
        public string self { get; set; }
        public string id { get; set; }
        public JiraUser author { get; set; }
        public string body { get; set; }
        public JiraUser updateauthor { get; set; }
        public string created { get; set; }
        public string updated { get; set; }
        public Dictionary<string, Dictionary<string, string>> visibility { get; set; }
    }
}
