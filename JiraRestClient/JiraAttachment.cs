﻿using System;
using System.Collections.Generic;

namespace JiraRestClient
{
    public class JiraAttachment
    {
        public string self { get; set; }
        public string id { get; set; }
        public string filename { get; set; }
        public JiraUser author { get; set; }
        public string created { get; set; }
        public int size { get; set; }
        public string mimetype { get; set; }
        public string content { get; set; }
        public string thumbnail { get; set; }
    }
}
