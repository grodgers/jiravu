﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace JiraRestClient
{
    public class JiraIssueFields : JiraRestClientBase
    {
        private List<JiraIssueLink> _issuelinks;
        private string _summary;

        public JiraComments comment { get; set; }
        public JiraIssue parent { get; set; }
        public JiraIssueType issuetype { get; set; }
        public JiraPriority priority { get; set; }
        public JiraProject project { get; set; }
        public JiraResolution resolution { get; set; }
        public JiraStatus status { get; set; }
        public JiraUser assignee { get; set; }
        public JiraUser reporter { get; set; }
        public List<JiraAttachment> attachment { get; set; }
        public List<JiraComponent> components { get; set; }
        public List<JiraIssue> subtasks { get; set; }
        public List<JiraIssueLink> issuelinks
        {
            get { return _issuelinks; }
            set { _issuelinks = value; RaisePropertyChanged("issuelinks"); }
        }
        public List<JiraVersion> fixVersions { get; set; }
        public List<JiraVersion> versions { get; set; }
        public string created { get; set; }
        public string description { get; set; }
        public string duedate { get; set; }
        public string resolutiondate { get; set; }
        public string summary
        {
            get { return _summary; }
            set { _summary = value; RaisePropertyChanged("summary"); }
        }
        public string updated { get; set; }
    }
}
