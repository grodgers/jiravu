﻿using System;
using System.Collections.Generic;

namespace JiraRestClient
{
    /// <summary>
    /// Used for creating and updating issues, provided any combination of allowable fields
    /// </summary>
    public class JiraIssueGeneric
    {
        public Dictionary<string, object> fields { get; set; }

        public JiraIssueGeneric()
        {
            this.fields = new Dictionary<string, object>();
        }
    }
}
