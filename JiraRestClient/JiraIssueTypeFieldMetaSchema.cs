﻿using System;
using System.Collections.Generic;

namespace JiraRestClient
{
    public class JiraIssueTypeFieldMetaSchema
    {
        public string type { get; set; }
        public string items { get; set; }
        public string system { get; set; }
        public string custom { get; set; }
        public string customId { get; set; }
    }
}
