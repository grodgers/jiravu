﻿using System;
using System.Collections.Generic;

namespace JiraRestClient
{
    public class JiraIssueLinkType
    {
        public string id { get; set; }
        public string self { get; set; }
        public string name { get; set; }
        public string inward { get; set; }
        public string outward { get; set; }
    }
}
