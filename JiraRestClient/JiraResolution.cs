﻿using System;

namespace JiraRestClient
{
    public class JiraResolution
    {
        public string self { get; set; }
        public string id { get; set; }
        public string description { get; set; }
        public string name { get; set; }
    }
}
