﻿using System;
using System.Collections.Generic;

namespace JiraRestClient
{
    public class JiraField
    {
        public string id { get; set; }
        public string name { get; set; }
        public bool custom { get; set; }
        public bool orderable { get; set; }
        public bool navigable { get; set; }
        public bool searchable { get; set; }
        public JiraFieldSchema schema { get; set; }
    }
}
