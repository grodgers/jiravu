﻿using System;

namespace JiraRestClient
{
    public class JiraVersion
    {
        public string self { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public bool archived { get; set; }
        public bool released { get; set; }
        public string releasedate { get; set; }

        public JiraVersion() { }
        public JiraVersion(string name)
        {
            this.name = name;
        }
    }
}
