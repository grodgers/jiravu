﻿using System;

namespace JiraRestClient
{
    public class JiraStatus
    {
        public string self { get; set; }
        public string description { get; set; }
        public string iconUrl { get; set; }
        public string name { get; set; }
        public string id { get; set; }

        public JiraStatus() { }
        public JiraStatus(string name)
        {
            this.name = name;
        }
    }
}
