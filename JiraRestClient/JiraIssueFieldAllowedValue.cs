﻿using System;
using System.Collections.Generic;

namespace JiraRestClient
{
    public class JiraIssueFieldAllowedValue
    {
        public string self { get; set; }
        public string id { get; set; }
        public string name { get; set; }
    }
}
