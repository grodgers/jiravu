﻿using System;
using System.Collections.Generic;

namespace JiraRestClient
{
    public class JiraUserSearchResponse
    {
        public int total { get; set; }
        public List<JiraUser> users { get; set; }
    }
}
