﻿using System;
using System.Collections.Generic;

namespace JiraRestClient
{
    /// <summary>
    /// Contains meta data for fields related to a particular issue type.
    /// </summary>
    public class JiraIssueTypeFieldMeta
    {
        public bool required { get; set; }
        public JiraIssueTypeFieldMetaSchema schema { get; set; }
        public string name { get; set; }
        public string autocompleteurl { get; set; }
        public List<string> operations { get; set; }
        public List<JiraIssueFieldAllowedValue> allowedvalues { get; set; }
    }
}
