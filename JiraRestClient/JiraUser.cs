﻿using System;
using System.Collections.Generic;

namespace JiraRestClient
{
    public class JiraUser
    {
        public string self { get; set; }
        public string name { get; set; }
        public string emailaddress { get; set; }
        public string displayname { get; set; }
        public bool active { get; set; }
        public Dictionary<string, string> avatarUrls { get; set; }

        public JiraUser() { }
        public JiraUser(string name)
        {
            this.name = name;
        }
    }
}
