﻿using System;
using System.Collections.Generic;

namespace JiraRestClient
{
    public class JiraComments
    {
        public int startat { get; set; }
        public int maxresults { get; set; }
        public int total { get; set; }
        public List<JiraComment> comments { get; set; }
    }
}
