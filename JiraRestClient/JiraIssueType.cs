﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace JiraRestClient
{
    public class JiraIssueType : JiraRestClientBase, IComparable
    {
        private string _iconUrl;

        public string self { get; set; }
        public string id { get; set; }
        public string description { get; set; }
        public string iconUrl
        {
            get { return _iconUrl;  }
            set { 
                _iconUrl = value;
                RaisePropertyChanged("iconUrl"); 
            }
        }
        public string name { get; set; }
        public bool subtask { get; set; }
        /// <summary>
        /// List of fields and related metadata from the createmeta response.
        /// Note: Must include expand=projects.issuetypes.fields in request to createmeta.
        /// </summary>
        public Dictionary<string, JiraIssueTypeFieldMeta> fields { get; set; }

        public JiraIssueType() { }
        public JiraIssueType(string name)
        {
            this.name = name;
        }
        public JiraIssueType(string name, bool subtask)
        {
            this.name = name;
            this.subtask = subtask;
        }

        public int CompareTo(object obj)
        {
            try
            {
                JiraIssueType toCompare = (JiraIssueType)obj;
                if (this.id == toCompare.id || this.name == toCompare.name) return 1;
            }
            catch (Exception) { }
            return 0;
        }

        public override bool Equals(object obj)
        {
            try
            {
                JiraIssueType toCompare = (JiraIssueType)obj;
                if (this.id == toCompare.id || this.name == toCompare.name) return true;
            }
            catch (Exception) { }
            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return this.name;
        }
    }
}
