﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Linq;
using System.Web.Script.Serialization;
using RestSharp;

namespace JiraRestClient
{
    public class JiraRestClient : JiraRestClientBase
    {
        RestClient client = null;
        RestRequest request = null;

        public string BaseUrl { get; set; }
        public string BasicAuthUsername { get; set; }
        public string BasicAuthPassword { get; set; }
        public string ServerUrl { get; set; }
        
        public JiraRestClient(string baseUrl, string username, string password)
        {
            BaseUrl = baseUrl;
            ServerUrl = baseUrl.Substring(0, baseUrl.IndexOf("rest"));
            client = new RestClient(baseUrl);
            client.Authenticator = new HttpBasicAuthenticator(username, password);
        }

        #region Private Methods

        private string FormatForHttp(string str)
        {
            str = str.Replace(' ', '+');
            return str;
        }

        #endregion

        #region Public Methods

        public JiraIssue CreateIssue(JiraIssueGeneric issueToCreate)
        {
            request = new RestRequest();
            request.AddHeader("Content-Type", "application/json");
            request.Method = Method.POST;
            request.Resource = "issue";
            request.RequestFormat = DataFormat.Json;
            request.AddBody(issueToCreate);
            IRestResponse response = client.Execute(request);
            JiraIssue issueCreated = new JavaScriptSerializer().Deserialize<JiraIssue>(response.Content);
            return issueCreated;
        }

        public JiraIssueCreateMetaResponse GetCreateIssueMeta()
        {
            request = new RestRequest();
            request.Resource = "issue/createmeta?expand=projects.issuetypes.fields";
            IRestResponse response = client.Execute(request);
            JiraIssueCreateMetaResponse metaResponse = 
                new JavaScriptSerializer().Deserialize<JiraIssueCreateMetaResponse>(response.Content);
            return metaResponse;
        }

        public JiraIssueCreateMetaResponse GetCreateIssueMeta(string projectKeys)
        {
            request = new RestRequest();
            request.Resource = "issue/createmeta?expand=projects.issuetypes.fields&projectKeys=" + projectKeys;
            IRestResponse response = client.Execute(request);
            JiraIssueCreateMetaResponse metaResponse =
                new JavaScriptSerializer().Deserialize<JiraIssueCreateMetaResponse>(response.Content);
            return metaResponse;
        }

        public JiraIssue GetIssueByID(string issueKeyOrId)
        {
            request = new RestRequest();
            request.Resource = "issue/" + issueKeyOrId;
            IRestResponse response = client.Execute(request);
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            // Deserialize the response into a JiraIssue object
            JiraIssue issue = serializer.Deserialize<JiraIssue>(response.Content);
            // Deserialize the response into a JiraIssueGeneric object
            JiraIssueGeneric issueGeneric = serializer.Deserialize<JiraIssueGeneric>(response.Content);
            // Populate JiraIssue.AllFields from the generic issue (this way the JiraIssue has explicit properties and all custom fields)
            issue.AllFields = issueGeneric.fields;
            return issue;
        }

        public List<JiraIssue> GetIssuesByJqlSearch(string jql, int startAt, int maxResults)
        {
            request = new RestRequest();
            request.Resource = String.Format("search?jql={0}&startAt={1}&maxResults={2}", FormatForHttp(jql), startAt, maxResults);
            IRestResponse response = client.Execute(request);
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = Properties.Settings.Default.MaxJsonLength;
            // Deserialize the response into a list of JiraIssue objects
            JiraJqlSearchResponse jqlSearchResponse = serializer.Deserialize<JiraJqlSearchResponse>(response.Content);
            
            if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
            {
                throw new ArgumentException(jqlSearchResponse.errorMessages[0]);
            }

            // Deserialize the response into a list of JiraIssueGeneric objects
            JiraJqlSearchResponseGeneric jqlSearchResponseGeneric = serializer.Deserialize<JiraJqlSearchResponseGeneric>(response.Content);
            if (jqlSearchResponse.issues == null) return new List<JiraIssue>();
            // Populate JiraIssue.AllFields from the generic issue (this way each JiraIssue has explicit properties and all custom fields)
            for (int i=0; i < jqlSearchResponse.issues.Count; i++)
            {
                jqlSearchResponse.issues[i].AllFields = jqlSearchResponseGeneric.issues[i].fields;
            }
            return jqlSearchResponse.issues;
        }

        public List<JiraIssueType> GetIssueTypes()
        {
            request = new RestRequest();
            request.Resource = "issuetype";
            IRestResponse response = client.Execute(request);
            List<JiraIssueType> types =
                new JavaScriptSerializer().Deserialize<List<JiraIssueType>>(response.Content);
            return types;
        }

        public List<JiraUser> GetUsersBySearch(string username)
        {
            request = new RestRequest();
            request.Resource = "user/search?username=" + username;
            IRestResponse response = client.Execute(request);

            if (response.ErrorException != null)
            {
                if (response.ErrorMessage.Contains("The remote name could not be resolved"))
                {
                    throw new ArgumentException("The URL provided is not valid, or a connection could not be established.");
                }
            }

            if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                throw new UnauthorizedAccessException("The credentials provided are invalid.");
            }

            List<JiraUser> users = new List<JiraUser>();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            try { users = serializer.Deserialize<List<JiraUser>>(response.Content); }
            catch (Exception) { users = new List<JiraUser>(); }
            return users;
        }

        public List<JiraStatus> GetStatuses()
        {
            request = new RestRequest();
            request.Resource = "status";
            IRestResponse response = client.Execute(request);
            List<JiraStatus> statuses =
                new JavaScriptSerializer().Deserialize<List<JiraStatus>>(response.Content);
            return statuses;
        }

        public bool UpdateIssue(string issueKeyOrId, JiraIssueGeneric issueToUpdate)
        {
            request = new RestRequest();
            request.AddHeader("Content-Type", "application/json");
            request.Method = Method.PUT;
            request.Resource = "issue/" + issueKeyOrId;
            request.RequestFormat = DataFormat.Json;
            request.AddBody(issueToUpdate);
            IRestResponse response = client.Execute(request);
            return response.Content == String.Empty;
        }

        #endregion
    }
}
