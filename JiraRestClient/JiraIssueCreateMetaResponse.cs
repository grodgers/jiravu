﻿using System;
using System.Collections.Generic;

namespace JiraRestClient
{
    public class JiraIssueCreateMetaResponse
    {
        #region Public Properties

        public List<JiraProject> projects { get; set; }

        #endregion

        #region Public Methods

        public JiraProject GetProject(string projectKey)
        {
            foreach (JiraProject proj in projects)
            {
                if (proj.key == projectKey) return proj;
            }

            return null;
        }

        public JiraIssueType GetIssueTypeForProject(string projectKey, string issueTypeName)
        {
            foreach (JiraProject project in projects)
            {
                if (project.key == projectKey)
                {
                    foreach (JiraIssueType issueType in project.issuetypes)
                    {
                        if (issueType.name == issueTypeName)
                        {
                            return issueType;
                        }
                    }
                }
            }

            return null;
        }

        public List<JiraIssueType> GetIssueTypesForProject(string projectName)
        {
            try
            {
                List<JiraIssueType> issueTypeList = null;
                foreach (JiraProject project in projects)
                {
                    if (project.name == projectName)
                    {
                        issueTypeList = project.issuetypes;
                        break;
                    }
                }
                return issueTypeList;
            }
            catch (Exception) { return null; }
        }

        #endregion
    }
}
