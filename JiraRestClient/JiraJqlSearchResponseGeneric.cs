﻿using System;
using System.Collections.Generic;

namespace JiraRestClient
{
    public class JiraJqlSearchResponseGeneric
    {
        public int maxresults { get; set; }
        public int total { get; set; }
        public List<JiraIssueGeneric> issues { get; set; }
    }
}
