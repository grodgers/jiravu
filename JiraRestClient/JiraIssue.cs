﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace JiraRestClient
{
    public class JiraIssue : JiraRestClientBase
    {
        private JiraIssueFields _fields;

        public string key { get; set; }
        public string id { get; set; }
        public string self { get; set; }
        public JiraIssueFields fields
        {
            get { return _fields; }
            set { _fields = value; RaisePropertyChanged("fields"); }
        }
        public Dictionary<string, object> AllFields { get; set; }

        public JiraIssue() { }
        public JiraIssue(string key)
        {
            this.key = key;
        }
    }
}
