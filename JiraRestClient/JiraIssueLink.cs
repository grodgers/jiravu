﻿using System;
using System.Collections.Generic;

namespace JiraRestClient
{
    public class JiraIssueLink : JiraRestClientBase
    {
        private JiraIssue _inwardissue;
        private JiraIssue _outwardissue;
        private JiraIssueLinkType _type;

        public string id { get; set; }
        public string self { get; set; }
        public JiraIssueLinkType type
        {
            get { return _type; }
            set { _type = value; RaisePropertyChanged("type"); }
        }
        public JiraIssue inwardissue
        {
            get { return _inwardissue; }
            set { _inwardissue = value; RaisePropertyChanged("inwardissue"); }
        }
        public JiraIssue outwardissue
        {
            get { return _outwardissue; }
            set { _outwardissue = value; RaisePropertyChanged("outwardissue"); }
        }
    }
}
