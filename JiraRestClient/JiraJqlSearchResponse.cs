﻿using System;
using System.Collections.Generic;

namespace JiraRestClient
{
    public class JiraJqlSearchResponse
    {
        public int maxresults { get; set; }
        public int total { get; set; }
        public List<JiraIssue> issues { get; set; }
        public List<string> errorMessages { get; set; }
    }
}
