﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace JiraRestClient.Test
{
    /// <summary>
    /// These tests assume a text file called jirarestauth.txt exists on the C: drive.
    /// The text file should have the base rest URL on the first line, then username 
    /// and password on the next two lines. This can be modified to meet your needs.
    /// </summary>
    [TestClass]
    public class UnitTest1
    {
        private string filePath = @"C:\jirarestauth.txt";

        [TestMethod]
        public void TestGetIssue()
        {
            string[] lines = System.IO.File.ReadAllLines(filePath);
            JiraRestClient client = new JiraRestClient(lines[0], lines[1], lines[2]);

            // Get a list of Jira issues matching the given query
            JiraIssue issue = client.GetIssueByID("JVX-1157");
            Assert.AreEqual(issue.key, "JTAB-72");
        }

        [TestMethod]
        public void TestCreateIssue()
        {
            string[] lines = System.IO.File.ReadAllLines(filePath);
            JiraRestClient client = new JiraRestClient(lines[0], lines[1], lines[2]);

            // Create a new generic issue object
            JiraIssueGeneric newIssue = new JiraIssueGeneric();
            newIssue.fields.Add("project", new JiraProject("PJVX"));
            newIssue.fields.Add("parent", new JiraIssue("PJVX-4156"));
            newIssue.fields.Add("issuetype", new JiraIssueType("Test Instance", true));
            newIssue.fields.Add("summary", "This issue was created by JiraVu");
            newIssue.fields.Add("assignee", new JiraUser("grodgers"));

            // Get the issue created
            JiraIssue issueCreated = client.CreateIssue(newIssue);
            Assert.AreNotEqual(issueCreated.key, null);
        }

        [TestMethod]
        public void TestUpdateIssue()
        {
            string[] lines = System.IO.File.ReadAllLines(filePath);
            JiraRestClient client = new JiraRestClient(lines[0], lines[1], lines[2]);

            // Create a new generic issue object
            JiraIssueGeneric updateIssue = new JiraIssueGeneric();
            updateIssue.fields.Add("description", "Description updated " + DateTime.Now.ToString());
            JiraIssue parentIssue = client.GetIssueByID("PJVX-4156");

            // Add versions to child issue from what currently exists in parent issue
            updateIssue.fields.Add("versions", parentIssue.fields.versions);

            // Update the issue
            Assert.IsTrue(client.UpdateIssue("PJVX-4157", updateIssue));
        }

        [TestMethod]
        public void TestGetIssuesFromJqlSearch()
        {
            string[] lines = System.IO.File.ReadAllLines(filePath);
            JiraRestClient client = new JiraRestClient(lines[0], lines[1], lines[2]);

            // Get a list of Jira issues matching the given query
            List<JiraIssue> issues = client.GetIssuesByJqlSearch("issuetype = Bug order by key", 0, 9999);
            Assert.AreEqual(issues.Count, 1992);
        }

        [TestMethod]
        public void TestGetCreateMetaData()
        {
            string[] lines = System.IO.File.ReadAllLines(filePath);
            JiraRestClient client = new JiraRestClient(lines[0], lines[1], lines[2]);

            // Get issue creation metadata, based on project keys, if specified; otherwise, get everything
            JiraIssueCreateMetaResponse response = null;
            if (lines[3] != String.Empty)
            {
                response = client.GetCreateIssueMeta(lines[3]);
            }
            else
            {
                response = client.GetCreateIssueMeta();

            }
            Assert.IsNotNull(response.projects);
        }

    }
}
